import { 
  StyleSheet, 
  Text, 
  View, 
  SafeAreaView,
  ImageBackground,
  Image,
  Dimensions,
  TouchableOpacity,
  ScrollView,
  Modal,
  Pressable,
} from 'react-native'
import React , {useState} from 'react'
import { SIZE } from '../../../../styles'

import CheckIcon from '../../../../assets/icons/check.svg'
 
const { width, height } = Dimensions.get('screen')

export default function Bording({navigation}) {
 
  const [popup, setPopup] = useState(false)
  const [isChecked, setIsChecked] = useState(false);
  const [showLorem, setShowLorem] = useState(false);
  const [showLearnMore, setShowLearnMore] = useState(true);

  const handleCheckboxClick = () => {
    setIsChecked(!isChecked);
  };

  const handleLearnMoreClick = () => {
    setShowLorem(true);
    setShowLearnMore(false);
  };
  return (
    <SafeAreaView style = {styles.main}>
      <View style = {styles.container}>
      <View style = {styles.top}>
        <Text style = {styles.topText}>Connect with the <Text style = {styles.topText1}>Family</Text></Text>
        <Text style = {styles.topText2}>Join to the number one family app</Text>
      </View>
      <View style = {styles.middle}>
          <Image style = {styles.img1} source={require("../../../../assets/images/Ellipse13.png")}/>
          <Image style = {styles.img2} source={require("../../../../assets/images/Ellipse6.png")}/>
          <Image style = {styles.img3} source={require("../../../../assets/images/Ellipse5.png")}/>
          <Image style = {styles.img4} source={require("../../../../assets/images/Ellipse20.png")}/>
          <Image style = {styles.img5} source={require("../../../../assets/images/Ellipse23.png")}/>
          <Image style = {styles.img6} source={require("../../../../assets/images/Ellipse19.png")}/>
          <Image style = {styles.img7} source={require("../../../../assets/images/Ellipse24.png")}/>
          <Image style = {styles.img8} source={require("../../../../assets/images/Ellipse22.png")}/>
        
      </View>
      <View style = {styles.bottom}>
        <TouchableOpacity 
          onPress={()=>setPopup(true)}
          activeOpacity={0.8} 
          style = {styles.btn}>
          <Text style = {styles.btnText}>Lets Start</Text>
        </TouchableOpacity>
      </View>
      <Modal
      transparent = {true}
      visible = {popup}
      >
        <ScrollView>
        <View style={styles.modalpopup}>
          <View style={styles.modalTop}>
            <Text style = {styles.modalText}>
              Privacy & Policy
            </Text>
            <Text style = {styles.modalText1}>
              Kindly Please follow our privacy and policies
            </Text>
            <Text style = {styles.modalText2}>
                This privacy policy has been compiled to better serve those who are concerned with how their 'Personally identifiable information'
                (PII) is being used online. PII, as used in Indian privacy law and information security, is information that can be used on its own
                or with other information toidentify, contact, or locate a single person, or to identify an individual in context. Please read
                our privacy policy carefully to get a clear understanding of how we collect, use , protect or otherwise handle 
                your Personally Identifiable Information in accordance with our website 
            </Text>
            <Text style = {styles.modalText3}>
                We may use the information we collect from you To personalize user's experience and to allow us to deliver the type of content
                and product offerings in which you are most interested. To improve our website in order to better serve you. To allow us to 
                better service you in responding to your customer service requests. To administer a contest, promotion, survey or other site 
                feature.{"\n"}To send periodic emails regarding your order or other products and services.
                {showLearnMore && (
        <Text style={styles.learnMore} onPress={handleLearnMoreClick}>
          Learn More
        </Text>
      )}
      {showLorem && (
        <Text style={styles.loremText}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        </Text>
      )}
            </Text>
          </View>
          <View style = {styles.modalBottom}>
            <View style={styles.checkboxContainer}>
              <TouchableOpacity
              activeOpacity={1}
              onPress={handleCheckboxClick} style={[styles.checkbox, {borderColor: isChecked ? '#459F56' : '#000'}]}>
                {isChecked && <CheckIcon height={23} width={23} />}
              </TouchableOpacity>
              <Text style = {styles.boxText}>
                Accept privacy and policy
              </Text>
            </View>
            <View style = {styles.agree}>
              <TouchableOpacity  onPress={() => navigation.navigate("SignUp")} activeOpacity={0.8} style = {styles.btn}>
                <Text style = {styles.btnText}>
                  I Agree
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        </ScrollView>
      </Modal>
      </View>
    </SafeAreaView>
  )
}

const styles = StyleSheet.create({
  main: {
    flex:1,
    backgroundColor:'#ffff',
  },
  container : {
    flexDirection:'column',
    justifyContent:'space-between',
  },
  top: {
    paddingTop: SIZE(40),
    paddingLeft:16,
  },
  topText: {
    fontSize: 32,
    fontFamily: 'Poppins-Regular',
    lineHeight: 40,
    fontWeight:'700',
    color:'#000',
    width:230,
  },
  topText1: {
    fontSize: 32,
    fontFamily: 'Poppins-Regular',
    lineHeight: 40,
    fontWeight:'700',
    color:'#459F56',
    // width:230,
    
  },
  topText2 :{
    fontSize: 16,
    fontWeight:'700',
  },
  middle : {
    flexDirection:"row",
    position: "relative",
  },
  img1 : {
    top: SIZE(100),
    position: "absolute",
  },
  img2 : {
    top: SIZE(20),
    left: 204.05,
    position: "absolute",
  },
  img3 : {
    top: SIZE(500),
    left:0,
    position: "absolute",
  },
  img4 : {
    top: SIZE(75),
    left:20,
    position: "absolute",
  },
  img5 : {
    top: SIZE(60),
    left:SIZE(260),
    position: "absolute",
  },
  img6 : {
    top: SIZE(230),
    left:SIZE(210),
    position: "absolute",
  },
  img8 : {
    top: SIZE(440),
    left:SIZE(320),
    position: "absolute",
    width:41.65,
  },
  img7 : {
    top: SIZE(480),
    left:50,
    position: "absolute",
    width:78.09,
  },
  bottom: {
    borderTopWidth:1 ,
    borderTopColor:"#D4D4D4",
    backgroundColor:'#ffff',
    paddingVertical: 10,
    paddingHorizontal:15,
    top:550,
    zIndex:1000,
  },
  btn: {
    backgroundColor:'#459F56',
    borderRadius:25,
    justifyContent:'center',
  },
  btnText: {
    color:'#ffff',
    fontSize: 16,
    fontWeight:'700',
    textAlign:'center',
    padding:15,
  },
  modalpopup: {
    backgroundColor:"#ffff",
    flex:1,
    paddingHorizontal:16,
  },
  modalTop : {
    paddingTop:40,
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    fontWeight:'700',
    fontSize: 20,
    color:'#000',
    textAlign:'center',
  },
  modalText1: {
    width: "70%",
    alignSelf: "center",
    fontFamily: 'Poppins-Regular',
    fontWeight: '700',
    fontSize: 14,
    color: '#747474',
    textAlign: 'center',
    paddingVertical: 15,
  },
  
  modalText2: {
    paddingTop:15,
    fontFamily: 'Poppins-Regular',
    fontWeight: '700',
    fontSize: 13,
    lineHeight:24,
    color: '#000',
  },
  modalText3: {
    paddingTop: 45,
    fontFamily: 'Poppins-Regular',
    fontWeight: '700',
    fontSize: 13,
    lineHeight: 24,
    color: '#000',
    paddingBottom: 65,
  },
  
  learnMore: {
    color: '#459F56',
    fontFamily: 'Poppins-Bold',
    fontWeight: '700',
    fontSize: 13,
    lineHeight: 24,
  },
  checkbox : {
    borderWidth: 1,
    width: 23,
    height: 23,
    borderRadius: 5,
    borderColor: '#D4D4D4',
    backgroundColor: '#fff',
    alignItems: 'center',
    borderColor: '#D4D4D4',
    backgroundColor: '#fff',
    alignItems: 'center',
  },
  checkboxContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom:30,
  },
  boxText: {
    marginLeft: 8,
    fontFamily: 'Poppins-Regular',
    fontWeight: '700',
    fontSize: 13.5,
    color: '#3D6A6E',
  },
  agree: {
    borderTopWidth:1 ,
    borderTopColor:"#D4D4D4",
    backgroundColor:'#ffff',
  paddingVertical:10,
  },
  loremText : {
    fontFamily: 'Poppins-Regular',
    fontWeight: '700',
    fontSize: 13,
    lineHeight: 24,
    color: '#000',
  },
})