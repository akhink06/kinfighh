import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  Image,
  Dimensions,
  TextInput,
  Modal,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useRef, useEffect} from 'react';
import { SIZE } from '../../../../styles';

import AsyncStorage from '@react-native-async-storage/async-storage';
import { useNavigation } from '@react-navigation/native'; 

import IndiaIcon from '../../../../assets/icons/indd.svg';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
const {width, height} = Dimensions.get('screen');
export default function SignUp({ handleLoginSuccess }) {
  const navigation = useNavigation();
  
  const [phoneNumber, setPhoneNumber] = useState('');

  const [modalVisible, setModalVisible] = useState(false);
  const [modalVisible1, setModalVisible1] = useState(false);

  const [otp, setOtp] = useState(Array(4).fill(''));

  const [isActive, setIsActive] = useState(false);

  const refs = useRef([]);

  const handleOtpChange = (index, text) => {
    const updatedOtp = [...otp];
    updatedOtp[index] = text;
    setOtp(updatedOtp);
    if (text.length > 0 && index < otp.length - 1) {
      refs.current[index + 1].focus();
    }
  };

  const generateUserId = () => {
    // Generate a random string of alphanumeric characters
    const characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    const userIdLength = 10; // Length of the user ID
    let userId = '';
  
    // Generate the user ID by randomly selecting characters from the characters string
    for (let i = 0; i < userIdLength; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      userId += characters.charAt(randomIndex);
    }
  
    return userId;
  };
  
  const verifylogin = async () => {
    if (phoneNumber.trim().length === 10) {
      const userId = generateUserId();
      await handleLoginSuccess(userId, phoneNumber, navigation);
    } else {
      console.error('Please enter a valid phone number');
    }
  };

  const handleSendOTP =()=>{
    setModalVisible(false)
    setModalVisible1(true)
}

  const updatePopup = action => {
    switch (action.type) {
      case 'CLOSE':
        setModalVisible1(false);
        break;
      default:
        break;
    }
  };

  const [seconds, setSeconds] = useState(20);
  const [countdownOver, setCountdownOver] = useState(false);

  useEffect(() => {
    const interval = setInterval(() => {
      if (seconds > 0) {
        setSeconds(prevSeconds => prevSeconds - 1);
      } else {
        setCountdownOver(true);
        clearInterval(interval); 
      }
    }, 1000);

    return () => clearInterval(interval);
  }, [seconds]);
  const handleResendOTP = () => {
    setSeconds(20);
    setCountdownOver(false);
  };
  return (
    <SafeAreaView style={styles.main}>
      <KeyboardAwareScrollView>
        <View style={styles.top}>
          <Image source={require('../../../../assets/images/group.png')} />
        </View>
        <View style={styles.middleTop}>
          <Text style={styles.middleTopTex}>
            Welcome to your world of{' '}
            <Text style={styles.middleTopTex1}>Family</Text>
          </Text>
          <Text style={styles.middleTopTex2}>Login with your phone number</Text>
        </View>
        <View style={styles.middleBottom}>
          <View style={styles.input}>
            <Image
              style={styles.inputImage}
              source={require('../../../../assets/images/indiaa.png')}
            />
            <Text style={styles.inputType}>+91</Text>
            <TextInput
              style={styles.inputField}
              placeholder="Enter Phone Number"
              keyboardType="phone-pad"
              value={phoneNumber}
              onChangeText={setPhoneNumber}
              maxLength={10}
              placeholderTextColor="#747474"
            />
          </View>
        </View>
        <TouchableOpacity
          activeOpacity={0.8}
          style={styles.bottom}
          onPress={() => setModalVisible(true)}>
          <Text style={styles.otp}>Send OTP</Text>
        </TouchableOpacity>
      </KeyboardAwareScrollView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        // onRequestClose={() => {
        //   setModalVisible(false);
        // }}
      >
        <View style={styles.modal}>
          <View style={styles.modalTop}>
            <Text style={styles.modalVerify}>Verify your Number</Text>
            <Text style={styles.modalOtp}>
              We will send an OTP to this number
            </Text>
            <Text style={styles.modalText}>{phoneNumber.toString()}</Text>
            <View style={styles.modalFlex}>
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                  setModalVisible(false);
                }}
                style={styles.closeButton}>
                <Text style={styles.closeButtonText}>EDIT</Text>
              </TouchableOpacity>

              <TouchableOpacity
                style={styles.closeButton}
                onPress={() => {handleSendOTP(),
                   updatePopup
                }}>
                <Text style={styles.closeButtonText}>CONTINUE</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
      <Modal transparent={true} visible={modalVisible1}>
        <View style={styles.innerModal}>
          <KeyboardAwareScrollView>
            <View style={styles.top}>
              <Image source={require('../../../../assets/images/group.png')} />
            </View>
            <View style={styles.middleTopmodal}>
              <Text style={styles.middleTopTex}>Verify</Text>
              <Text style={styles.middleTopTex2}>
                Please enter the 4 digit verification code that is send to you
                at{' '}
                <Text style={{color: 'green'}}>{phoneNumber.toString()}</Text>
              </Text>
            </View>
            <View style={styles.middleBottom}>
              <View style={styles.otpInputContainer}>
                {otp.map((_, index) => (
                  <TextInput
                    key={index}
                    ref={input => (refs.current[index] = input)}
                    style={[
                      styles.otpInput,
                      isActive ? styles.activeInput : styles.inactiveInput,
                    ]}
                    keyboardType="numeric"
                    maxLength={1}
                    onChangeText={text => handleOtpChange(index, text)}
                    value={otp[index]}
                    placeholder="0"
                    placeholderTextColor="#747474"
                    onFocus={() => setIsActive(true)}
                    onBlur={() => setIsActive(false)}
                  />
                ))}
              </View>
              <View style={styles.resend}>
                {countdownOver ? (
                  <TouchableOpacity onPress={handleResendOTP}>
                    <Text
                      style={{color: 'green', fontSize: 12, fontWeight: '700'}}>
                      Resend OTP
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <Text
                    style={{color: '#747474', fontSize: 12, fontWeight: '700'}}>
                    Don’t receive code?{' '}
                    <Text
                      style={{color: 'green', fontSize: 12, fontWeight: '700'}}>
                      {seconds >= 60
                        ? `${Math.floor(seconds / 60)}:${(
                            '0' +
                            (seconds % 60)
                          ).slice(-2)}`
                        : seconds}{' '}
                      sec
                    </Text>
                  </Text>
                )}
              </View>
            </View>
            <TouchableOpacity activeOpacity={0.8} onPress={verifylogin} style={styles.innerBottom} 
            >
              <Text style={styles.verify}>Verify</Text>
            </TouchableOpacity>
            <View style={styles.endBottom}>
            <Text style={styles.endBottomText}>
            If you are facing any issues to get the OTP, kindly Whatsapp to <Text  style={{color: 'green', fontSize: 12, fontWeight: '700'}}> +91 7403 115 357</Text>
            </Text>
            </View>
           
          </KeyboardAwareScrollView>
        </View>
      </Modal>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#ffff',
    paddingHorizontal: SIZE(20),
  },
  top: {
    alignItems: 'center',
    paddingVertical: SIZE(50),
  },
  middleTop: {
    width: '90%',
    paddingBottom: SIZE(50),
  },
  middleTopTex: {
    fontSize: 30,
    fontFamily: 'Poppins-Regular',
    lineHeight: 40,
    fontWeight: '700',
    color: '#000',
  },
  middleTopTex1: {
    fontSize: 30,
    fontFamily: 'Poppins-Regular',
    lineHeight: 40,
    fontWeight: '700',
    color: '#459F56',
  },
  middleTopTex2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#747474',
    paddingTop: SIZE(5),
  },

  middleBottom: {
    width: '100%',
  },
  input: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 99,
    borderColor: '#E4E5E8',
    padding: SIZE(12),
  },
  inputImage: {
    width: 34,
    height: 34,
    borderRadius: 99,
  },
  inputField: {
    paddingVertical: 0,
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000',
    width: '90%',
  },
  inputType: {
    marginLeft: SIZE(10),
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000',
  },
  bottom: {
    width: '100%',
    backgroundColor: '#459F56',
    borderRadius: 25,
    justifyContent: 'center',
    marginVertical: SIZE(50),
  },
  otp: {
    color: '#ffff',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
    padding: SIZE(15),
  },
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  modalTop: {
    backgroundColor: '#ffff',
    paddingHorizontal: SIZE(25),
    paddingVertical: SIZE(25),
    width: '90%',
    borderRadius: 5,
  },
  modalText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#459F56',
    marginBottom: SIZE(30),
    paddingTop: SIZE(5),
  },
  modalVerify: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000',
  },
  modalOtp: {
    paddingTop: SIZE(10),
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#747474',
  },
  modalFlex: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  closeButton: {
  },
  closeButtonText: {
    fontSize: 16,
    fontWeight: '700',
    color: '#459F56',
  },
  innerModal: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  innerBottom: {
    backgroundColor: '#459F56',
    borderRadius: 25,
    justifyContent: 'center',
    marginHorizontal: SIZE(20),
  },
  verify: {
    color: '#ffff',
    fontSize: 16,
    fontWeight: '700',
    textAlign: 'center',
    padding: SIZE(15),
  },
  otpContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: SIZE(10),
  },
  otpDigit: {
    width: 40,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },

  otpInputContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: SIZE(20),
  },
  otpInput: {
    width: 72,
    height: 72,
    borderRadius: 60,
    borderWidth: 1,
    textAlign: 'center',
    fontSize: 26,
    fontWeight: '700',
    color: '#000',
    backgroundColor: '#ffff',
    textAlign: 'center',
  },
  activeInput: {
    borderColor: '#459F56',
  },
  inactiveInput: {
    borderColor: '#D4D4D4',
  },
  resend: {
    marginTop: SIZE(15),
    marginBottom: SIZE(20),
    paddingHorizontal: SIZE(30),
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  middleTopmodal : {
    width: '100%',
    paddingBottom: SIZE(50),
    paddingHorizontal: SIZE(30),
  },
  endBottom : {
    width: '100%',
    paddingVertical : SIZE(10),
    paddingHorizontal: SIZE(20),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems:'center',
  },
  endBottomText :{
    width: '90%',
    lineHeight: 20,
    textAlign: 'center',
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    fontWeight: '700',
    color: '#747474',
  },
});
