import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  FlatList,
  Image,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import React from 'react';

import Card from './Inner_components/Card';
import LogoIcon from '../../../../assets/icons/logo.svg';
import SearchIcon from '../../../../assets/icons/search.svg';
import { SIZE } from '../../../../styles';

const Story = [
  {
    id: 1,
    storyImg: require('../../../../assets/images/plus.png'),
    name: 'Your moment',
  },
  {
    id: 2,
    profilePic: require('../../../../assets/images/john.png'),
    storyImg: require('../../../../assets/images/johnstory.png'),
    name: 'Abraham John',
  },
  {
    id: 3,
    profilePic: require('../../../../assets/images/anjima.png'),
    storyImg: require('../../../../assets/images/anjimastory.png'),
    name: 'Anjima',
  },
  {
    id: 4,
    profilePic: require('../../../../assets/images/devid.png'),
    storyImg: require('../../../../assets/images/devidstory.png'),
    name: 'Manu Devid',
  },
  {
    id: 5,
    profilePic: require('../../../../assets/images/john.png'),
    storyImg: require('../../../../assets/images/johnstory.png'),
    name: 'Abraham John',
  },
  {
    id: 6,
    profilePic: require('../../../../assets/images/anjima.png'),
    storyImg: require('../../../../assets/images/anjimastory.png'),
    name: 'Anjima',
  },
  {
    id: 7,
    profilePic: require('../../../../assets/images/devid.png'),
    storyImg: require('../../../../assets/images/devidstory.png'),
    name: 'Manu Devid',
  },
  {
    id: 8,
    profilePic: require('../../../../assets/images/john.png'),
    storyImg: require('../../../../assets/images/johnstory.png'),
    name: 'Abraham John',
  },
];
export default function Feeds() {
  return (
    <SafeAreaView style={styles.main}>
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerLogo}>
            <LogoIcon />
          </View>
          <View style={styles.headerSearch}>
            <SearchIcon />
          </View>
        </View>
        <ScrollView>
          <View style={styles.body}>
            <FlatList
              style={styles.row}
              data={Story}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              renderItem={({item, index}) => (
                <TouchableOpacity style={styles.story} activeOpacity={0.8}>
                  <View
                    style={[styles.storyProfile, index === 0 && styles.dash]}>
                    <View
                      style={[
                        styles.storyProfileStory,
                        index === 0 && styles.firstStory,
                      ]}>
                      <Image source={item.storyImg} />
                    </View>
                    <View style={styles.storyProfilePic}>
                      <Image
                        style={styles.storyProfilePicImg}
                        source={item.profilePic}
                      />
                    </View>
                    <View style={styles.storyProfileName}>
                      <Text style={styles.storyProfileNameText}>
                        {item.name}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
          <View>{<Card />}</View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  container: {
    flex: 1,
  },
  header: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: SIZE(20),
    paddingHorizontal: SIZE(20),
  },
  headerLogo: {},
  headerSearch: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  body: {
    // flex: 1,
    paddingLeft: SIZE(20),
  },
  story: {
    marginRight: SIZE(10),
    flexDirection: 'row',
  },

  storyProfile: {
    position: 'relative',
  },
  dash: {
    height: 112,
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#EEEEEE',
    padding: 1,
  },
  storyProfileStory: {
    borderWidth: 1,
    width: 84,
    height: 112,
    borderRadius: 5,
    borderColor: '#45A54C',
    alignItems: 'center',
    justifyContent: 'center',
  },
  firstStory: {
    borderStyle: 'dashed',
    borderColor: '#EEEEEE',
    height: 108,
  },
  storyProfilePic: {
    position: 'absolute',
    top: 9,
    left: 9,
    borderColor: '#ffff',
    width: 25,
    height: 25,
    borderRadius: 99,
    borderWidth: 2,
  },
  storyProfilePicImg: {
    width: 24,
    height: 24,
    flexDirection: 'row',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  storyProfileName: {
    // width : 76 ,
  },
  storyProfileNameText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    fontWeight: '700',
    color: '#707070',
    textAlign: 'center',
    width: 84,
  },
});
