import {
  FlatList,
  Image,
  Modal,
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Animated,
  ScrollView,
} from 'react-native';
import React, {useState} from 'react';
import {SIZE} from '../../../../../styles';

import MoreIcon from '../../../../../assets/icons/more.svg';
import LikeIcon from '../../../../../assets/icons/Like.svg';
import HeartIcon from '../../../../../assets/icons/Heart.svg';
import CommentIcon from '../../../../../assets/icons/Comment.svg';
import ScrollerIcon from '../../../../../assets/icons/Scroller.svg';
import RightArrowIcon from '../../../../../assets/icons/Arrow.svg';
import CloseIcon from '../../../../../assets/icons/close.svg';

const Post = [
  {
    id: 1,
    commentId: 1,
    wish1 : ' Birthday Celebration',
    wish2 : ' amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui eget justo tincidunt amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui',
    propic: require('../../../../../assets/images/Maccoy.png'),
    username: 'David Richard',
    userplace: 'Thekkan Family',
    postimg: require('../../../../../assets/images/Rec.png'),
    likeimg: <LikeIcon />,
    like: 1499,
    commentimg: <CommentIcon />,
    comment: 879,
    comment1: 29,
    text1: 'Birthday Celebration ',
    text2:
      'amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui eget justo tincidunt amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui eget justo tincidunt',
    commenterPropic: require('../../../../../assets/images/Maccoy.png'),
    commentername: 'Corina McCoy',
    commentdate: '2 days ago',
    close: <CloseIcon />,
    commenting:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic1: require('../../../../../assets/images/Alex.png'),
    commentername1: 'Alex Buckmaster',
    commentdate1: '2 days ago',
    rply1: '--- View 13 replay',
    relpyPropic1: require('../../../../../assets/images/Philips.png'),
    relpyPropic11: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic21: require('../../../../../assets/images/Alex.png'),
    relpyPropic31: require('../../../../../assets/images/pro.png'),
    replyname1: 'Tintu_Mwon',
    replemassege1: 'wow beautiful',
    replyname11: '__Cal_Shazham',
    replemassege11: 'cute....',
    replyname21: 'Karthik__Surya',
    replemassege21: 'happy family',
    replyname31: 'Aswathy__Achu',
    replemassege31: 'always be happy',
    arrow1: require('../../../../../assets/images/Arrow.png'),
    close1: <CloseIcon />,
    commenting1:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
    commenterPropic2: require('../../../../../assets/images/Philips.png'),
    commentername2: 'Autumn Philips',
    commentdate2: '2 days ago',
    rply2: '--- View 44 replay',
    relpyPropic32: require('../../../../../assets/images/Philips.png'),
    relpyPropic22: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic12: require('../../../../../assets/images/Alex.png'),
    relpyPropic2: require('../../../../../assets/images/pro.png'),
    replyname2: 'Aswathy_achu',
    replemassege2: 'wow beautiful',
    replyname12: '__Nihal123',
    replemassege12: 'cute....',
    replyname22: 'Mallu_boy',
    replemassege22: 'happy family',
    replyname32: 'silent_killer',
    replemassege32: 'always be happy',
    arrow2: require('../../../../../assets/images/Arrow.png'),
    close2: <CloseIcon />,
    commenting2:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic3: require('../../../../../assets/images/Maccoy.png'),
    commentername3: 'Corina McCoy',
    commentdate3: '2 days ago',
    close3: <CloseIcon />,
    commenting3:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic4: require('../../../../../assets/images/Maccoy.png'),
    commentername4: 'Corina McCoy',
    commentdate4: '2 days ago',
    rply4: '--- View 22 replay',
    relpyPropic4: require('../../../../../assets/images/Philips.png'),
    relpyPropic14: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic24: require('../../../../../assets/images/Alex.png'),
    relpyPropic34: require('../../../../../assets/images/pro.png'),
    replyname4: 'aysha__Marva',
    replemassege4: 'wow beautiful',
    replyname14: '__Arun_Smokkie',
    replemassege14: 'cute....',
    replyname24: 'Aswanth_Kok',
    replemassege24: 'happy family',
    replyname34: 'Arj_You',
    replemassege34: 'always be happy',
    arrow4: require('../../../../../assets/images/Arrow.png'),
    close4: <CloseIcon />,
    commenting4:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic5: require('../../../../../assets/images/Alex.png'),
    commentername5: 'Alex Buckmaster',
    commentdate5: '2 days ago',
    close: <CloseIcon />,
    commenting5:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
    commenterPropic6: require('../../../../../assets/images/Philips.png'),
    commentername6: 'Autumn Philips',
    commentdate6: '2 days ago',
    rply6: '--- View 30 replay',
    relpyPropic36: require('../../../../../assets/images/Philips.png'),
    relpyPropic26: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic16: require('../../../../../assets/images/Alex.png'),
    relpyPropic6: require('../../../../../assets/images/pro.png'),
    replyname6: 'Autumn__Philips',
    replemassege6: 'wow beautiful',
    replyname16: '__Arun_Photophile',
    replemassege16: 'cute....',
    replyname26: 'Ajmal_the_devil',
    replemassege26: 'happy family',
    replyname36: 'wanderlust__',
    replemassege36: 'always be happy',
    arrow6: require('../../../../../assets/images/Arrow.png'),
    close6: <CloseIcon />,
    commenting6:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic7: require('../../../../../assets/images/Maccoy.png'),
    commentername7: 'Corina McCoy',
    commentdate7: '2 days ago',
    rply7: '--- View 19 replay',
    relpyPropic7: require('../../../../../assets/images/Philips.png'),
    relpyPropic17: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic27: require('../../../../../assets/images/Alex.png'),
    relpyPropic37: require('../../../../../assets/images/pro.png'),
    replyname7: 'call_me_hari',
    replemassege7: 'wow beautiful',
    replyname17: '__devil_kunju',
    replemassege17: 'cute....',
    replyname27: 'the_devil',
    replemassege27: 'happy family',
    replyname37: 'daddys_princess',
    replemassege37: 'always be happy',
    arrow7: require('../../../../../assets/images/Arrow.png'),
    close7: <CloseIcon />,
    commenting7:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
  {
    id: 2,
    commentId: 2,
    wish1 : 'HAPPY 12TH ANNIVERSARY BABE',
    wish2 : ' THIS IS THE I WANT TO SAY EVRTHING THAT I AM HAPPY FOR BEEING WITH YOU ETEC EGTEVEVGMGBVMGMMM , ',
    text1: 'HAPPY 12TH ANNIVERSARY BABE',
    text2:
      'THIS IS THE I WANT TO SAY EVRTHING THAT I AM HAPPY FOR BEEING WITH YOU ETEC EGTEVEVGMGBVMGMMM , ',
    
    propic: require('../../../../../assets/images/Philips.png'),
    username: 'Corina McCoy',
    userplace: 'Thekkan Family',
    postimg: require('../../../../../assets/images/Rec1.png'),
    likeimg: <LikeIcon />,
    like: 1999,
    commentimg: <CommentIcon />,
    comment: 879,
    comment1: 35,
    commenterPropic: require('../../../../../assets/images/Maccoy.png'),
    commentername: 'Aswathy Achu',
    commentdate: '4 days ago',
    close: <CloseIcon />,
    commenting:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic1: require('../../../../../assets/images/Alex.png'),
    commentername1: 'Sahadh__',
    commentdate1: '1 days ago',
    rply1: '--- View 4 replay',
    relpyPropic1: require('../../../../../assets/images/Alex.png'),
    relpyPropic11: require('../../../../../assets/images/pro.png'),
    relpyPropic21: require('../../../../../assets/images/Philips.png'),
    relpyPropic31: require('../../../../../assets/images/Maccoy.png'),
    replyname1: 'Raseen Moh',
    replemassege1: 'wow beautiful',
    replyname11: '__Amal k Anil',
    replemassege11: 'cute....',
    replyname21: 'Shabana_Thasni',
    replemassege21: 'happy family',
    replyname31: 'Fida_k',
    replemassege31: 'always be happy',
    arrow1: require('../../../../../assets/images/Arrow.png'),
    close1: <CloseIcon />,
    commenting1:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
    commenterPropic2: require('../../../../../assets/images/Philips.png'),
    commentername2: 'ALnazeem',
    commentdate2: '2 days ago',
    rply2: '--- View 12 replay',
    relpyPropic32: require('../../../../../assets/images/Philips.png'),
    relpyPropic22: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic12: require('../../../../../assets/images/Alex.png'),
    relpyPropic2: require('../../../../../assets/images/pro.png'),
    replyname2: 'Aswathy_achu',
    replemassege2: 'wow beautiful',
    replyname12: '__Nihal123',
    replemassege12: 'cute....',
    replyname22: 'Mallu_boy',
    replemassege22: 'happy family',
    replyname32: 'silent_killer',
    replemassege32: 'always be happy',
    arrow2: require('../../../../../assets/images/Arrow.png'),
    close2: <CloseIcon />,
    commenting2:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic3: require('../../../../../assets/images/Maccoy.png'),
    commentername3: 'Ajsal_kT',
    commentdate3: '2 days ago',
    close3: <CloseIcon />,
    commenting3:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic4: require('../../../../../assets/images/Maccoy.png'),
    commentername4: 'Thansi_Ahammad',
    commentdate4: '2 days ago',
    rply4: '--- View 22 replay',
    relpyPropic4: require('../../../../../assets/images/Philips.png'),
    relpyPropic14: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic24: require('../../../../../assets/images/Alex.png'),
    relpyPropic34: require('../../../../../assets/images/pro.png'),
    replyname4: 'Jaseel_vk',
    replemassege4: 'wow beautiful',
    replyname14: '__Mahshuk',
    replemassege14: 'cute....',
    replyname24: 'Ziyadh__',
    replemassege24: 'happy family',
    replyname34: 'Sobir',
    replemassege34: 'always be happy',
    arrow4: require('../../../../../assets/images/Arrow.png'),
    close4: <CloseIcon />,
    commenting4:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic5: require('../../../../../assets/images/Alex.png'),
    commentername5: 'Noufal__',
    commentdate5: '2 days ago',
    close: <CloseIcon />,
    commenting5:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
    commenterPropic6: require('../../../../../assets/images/Philips.png'),
    commentername6: 'Leonal_Messi',
    commentdate6: '2 days ago',
    rply6: '--- View 10 replay',
    relpyPropic36: require('../../../../../assets/images/Philips.png'),
    relpyPropic26: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic16: require('../../../../../assets/images/Alex.png'),
    relpyPropic6: require('../../../../../assets/images/pro.png'),
    replyname6: 'Autumn__Philips',
    replemassege6: 'wow beautiful',
    replyname16: '__Arun_Photophile',
    replemassege16: 'cute....',
    replyname26: 'Ajmal_the_devil',
    replemassege26: 'happy family',
    replyname36: 'wanderlust__',
    replemassege36: 'always be happy',
    arrow6: require('../../../../../assets/images/Arrow.png'),
    close6: <CloseIcon />,
    commenting6:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic7: require('../../../../../assets/images/Maccoy.png'),
    commentername7: 'CR007',
    commentdate7: '2 days ago',
    rply7: '--- View 19 replay',
    relpyPropic7: require('../../../../../assets/images/Philips.png'),
    relpyPropic17: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic27: require('../../../../../assets/images/Alex.png'),
    relpyPropic37: require('../../../../../assets/images/pro.png'),
    replyname7: 'call_me_hari',
    replemassege7: 'wow beautiful',
    replyname17: '__devil_kunju',
    replemassege17: 'cute....',
    replyname27: 'the_devil',
    replemassege27: 'happy family',
    replyname37: 'daddys_princess',
    replemassege37: 'always be happy',
    arrow7: require('../../../../../assets/images/Arrow.png'),
    close7: <CloseIcon />,
    commenting7:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
  {
    id: 3,
    commentId: 3,
    wish1 : ' ITS OUR NEW HOME',
    wish2 : 'IT WAS OUR DREAM PROJECT , years of hard work volutpat lacinia arcu nibh vexl ante. Proin dapibus dui eget justo tincidunt amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui',
    
    propic: require('../../../../../assets/images/Alex.png'),
    username: 'Alex Buckmaster',
    userplace: 'Thekkan Family',
    postimg: require('../../../../../assets/images/Rec2.png'),
    likeimg: <LikeIcon />,
    like: 1110,
    commentimg: <CommentIcon />,
    comment: 879,
    comment1: 18,
    text1: 'Birthday Celebration ',
    text2:
      ' amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui eget justo tincidunt amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui eget justo tincidunt',
    commenterPropic: require('../../../../../assets/images/Maccoy.png'),
    commentername: 'Corina McCoy',
    commentdate: '2 days ago',
    close: <CloseIcon />,
    commenting:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic1: require('../../../../../assets/images/Alex.png'),
    commentername1: 'Alex Buckmaster',
    commentdate1: '2 days ago',
    rply1: '--- View 13 replay',
    relpyPropic1: require('../../../../../assets/images/Philips.png'),
    relpyPropic11: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic21: require('../../../../../assets/images/Alex.png'),
    relpyPropic31: require('../../../../../assets/images/pro.png'),
    replyname1: 'Tintu_Mwon',
    replemassege1: 'wow beautiful',
    replyname11: '__Cal_Shazham',
    replemassege11: 'cute....',
    replyname21: 'Karthik__Surya',
    replemassege21: 'happy family',
    replyname31: 'Aswathy__Achu',
    replemassege31: 'always be happy',
    arrow1: require('../../../../../assets/images/Arrow.png'),
    close1: <CloseIcon />,
    commenting1:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
    commenterPropic2: require('../../../../../assets/images/Philips.png'),
    commentername2: 'Autumn Philips',
    commentdate2: '2 days ago',
    rply2: '--- View 44 replay',
    relpyPropic32: require('../../../../../assets/images/Philips.png'),
    relpyPropic22: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic12: require('../../../../../assets/images/Alex.png'),
    relpyPropic2: require('../../../../../assets/images/pro.png'),
    replyname2: 'Aswathy_achu',
    replemassege2: 'wow beautiful',
    replyname12: '__Nihal123',
    replemassege12: 'cute....',
    replyname22: 'Mallu_boy',
    replemassege22: 'happy family',
    replyname32: 'silent_killer',
    replemassege32: 'always be happy',
    arrow2: require('../../../../../assets/images/Arrow.png'),
    close2: <CloseIcon />,
    commenting2:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic3: require('../../../../../assets/images/Maccoy.png'),
    commentername3: 'Corina McCoy',
    commentdate3: '2 days ago',
    close3: <CloseIcon />,
    commenting3:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic4: require('../../../../../assets/images/Maccoy.png'),
    commentername4: 'Corina McCoy',
    commentdate4: '2 days ago',
    rply4: '--- View 22 replay',
    relpyPropic4: require('../../../../../assets/images/Philips.png'),
    relpyPropic14: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic24: require('../../../../../assets/images/Alex.png'),
    relpyPropic34: require('../../../../../assets/images/pro.png'),
    replyname4: 'aysha__Marva',
    replemassege4: 'wow beautiful',
    replyname14: '__Arun_Smokkie',
    replemassege14: 'cute....',
    replyname24: 'Aswanth_Kok',
    replemassege24: 'happy family',
    replyname34: 'Arj_You',
    replemassege34: 'always be happy',
    arrow4: require('../../../../../assets/images/Arrow.png'),
    close4: <CloseIcon />,
    commenting4:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic5: require('../../../../../assets/images/Alex.png'),
    commentername5: 'Alex Buckmaster',
    commentdate5: '2 days ago',
    close: <CloseIcon />,
    commenting5:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
    commenterPropic6: require('../../../../../assets/images/Philips.png'),
    commentername6: 'Autumn Philips',
    commentdate6: '2 days ago',
    rply6: '--- View 30 replay',
    relpyPropic36: require('../../../../../assets/images/Philips.png'),
    relpyPropic26: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic16: require('../../../../../assets/images/Alex.png'),
    relpyPropic6: require('../../../../../assets/images/pro.png'),
    replyname6: 'Autumn__Philips',
    replemassege6: 'wow beautiful',
    replyname16: '__Arun_Photophile',
    replemassege16: 'cute....',
    replyname26: 'Ajmal_the_devil',
    replemassege26: 'happy family',
    replyname36: 'wanderlust__',
    replemassege36: 'always be happy',
    arrow6: require('../../../../../assets/images/Arrow.png'),
    close6: <CloseIcon />,
    commenting6:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic7: require('../../../../../assets/images/Maccoy.png'),
    commentername7: 'Corina McCoy',
    commentdate7: '2 days ago',
    rply7: '--- View 19 replay',
    relpyPropic7: require('../../../../../assets/images/Philips.png'),
    relpyPropic17: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic27: require('../../../../../assets/images/Alex.png'),
    relpyPropic37: require('../../../../../assets/images/pro.png'),
    replyname7: 'call_me_hari',
    replemassege7: 'wow beautiful',
    replyname17: '__devil_kunju',
    replemassege17: 'cute....',
    replyname27: 'the_devil',
    replemassege27: 'happy family',
    replyname37: 'daddys_princess',
    replemassege37: 'always be happy',
    arrow7: require('../../../../../assets/images/Arrow.png'),
    close7: <CloseIcon />,
    commenting7:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
  {
    id: 4,
    commentId: 4,
    propic: require('../../../../../assets/images/Philips.png'),
    username: 'Autumn Philips',
    userplace: 'Thekkan Family',
    postimg: require('../../../../../assets/images/Rec3.png'),
    likeimg: <LikeIcon />,
    heartimg: <HeartIcon />,
    like: 759,
    commentimg: <CommentIcon />,
    comment: 879,
    comment1: 16,
    commenterPropic: require('../../../../../assets/images/Maccoy.png'),
    commentername: 'Corina McCoy',
    commentdate: '2 days ago',
    close: <CloseIcon />,
    commenting:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic1: require('../../../../../assets/images/Alex.png'),
    commentername1: 'Sahadh__',
    commentdate1: '1 days ago',
    rply1: '--- View 4 replay',
    relpyPropic1: require('../../../../../assets/images/Alex.png'),
    relpyPropic11: require('../../../../../assets/images/pro.png'),
    relpyPropic21: require('../../../../../assets/images/Philips.png'),
    relpyPropic31: require('../../../../../assets/images/Maccoy.png'),
    replyname1: 'Raseen Moh',
    replemassege1: 'wow beautiful',
    replyname11: '__Amal k Anil',
    replemassege11: 'cute....',
    replyname21: 'Shabana_Thasni',
    replemassege21: 'happy family',
    replyname31: 'Fida_k',
    replemassege31: 'always be happy',
    arrow1: require('../../../../../assets/images/Arrow.png'),
    close1: <CloseIcon />,
    commenting1:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
    commenterPropic2: require('../../../../../assets/images/Philips.png'),
    commentername2: 'ALnazeem',
    commentdate2: '2 days ago',
    rply2: '--- View 12 replay',
    relpyPropic32: require('../../../../../assets/images/Philips.png'),
    relpyPropic22: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic12: require('../../../../../assets/images/Alex.png'),
    relpyPropic2: require('../../../../../assets/images/pro.png'),
    replyname2: 'Aswathy_achu',
    replemassege2: 'wow beautiful',
    replyname12: '__Nihal123',
    replemassege12: 'cute....',
    replyname22: 'Mallu_boy',
    replemassege22: 'happy family',
    replyname32: 'silent_killer',
    replemassege32: 'always be happy',
    arrow2: require('../../../../../assets/images/Arrow.png'),
    close2: <CloseIcon />,
    commenting2:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic3: require('../../../../../assets/images/Maccoy.png'),
    commentername3: 'Ajsal_kT',
    commentdate3: '2 days ago',
    close3: <CloseIcon />,
    commenting3:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic4: require('../../../../../assets/images/Maccoy.png'),
    commentername4: 'Thansi_Ahammad',
    commentdate4: '2 days ago',
    rply4: '--- View 22 replay',
    relpyPropic4: require('../../../../../assets/images/Philips.png'),
    relpyPropic14: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic24: require('../../../../../assets/images/Alex.png'),
    relpyPropic34: require('../../../../../assets/images/pro.png'),
    replyname4: 'Jaseel_vk',
    replemassege4: 'wow beautiful',
    replyname14: '__Mahshuk',
    replemassege14: 'cute....',
    replyname24: 'Ziyadh__',
    replemassege24: 'happy family',
    replyname34: 'Sobir',
    replemassege34: 'always be happy',
    arrow4: require('../../../../../assets/images/Arrow.png'),
    close4: <CloseIcon />,
    commenting4:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic5: require('../../../../../assets/images/Alex.png'),
    commentername5: 'Noufal__',
    commentdate5: '2 days ago',
    close: <CloseIcon />,
    commenting5:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
    commenterPropic6: require('../../../../../assets/images/Philips.png'),
    commentername6: 'Leonal_Messi',
    commentdate6: '2 days ago',
    rply6: '--- View 10 replay',
    relpyPropic36: require('../../../../../assets/images/Philips.png'),
    relpyPropic26: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic16: require('../../../../../assets/images/Alex.png'),
    relpyPropic6: require('../../../../../assets/images/pro.png'),
    replyname6: 'Autumn__Philips',
    replemassege6: 'wow beautiful',
    replyname16: '__Arun_Photophile',
    replemassege16: 'cute....',
    replyname26: 'Ajmal_the_devil',
    replemassege26: 'happy family',
    replyname36: 'wanderlust__',
    replemassege36: 'always be happy',
    arrow6: require('../../../../../assets/images/Arrow.png'),
    close6: <CloseIcon />,
    commenting6:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
    commenterPropic7: require('../../../../../assets/images/Maccoy.png'),
    commentername7: 'CR007',
    commentdate7: '2 days ago',
    rply7: '--- View 19 replay',
    relpyPropic7: require('../../../../../assets/images/Philips.png'),
    relpyPropic17: require('../../../../../assets/images/Maccoy.png'),
    relpyPropic27: require('../../../../../assets/images/Alex.png'),
    relpyPropic37: require('../../../../../assets/images/pro.png'),
    replyname7: 'call_me_hari',
    replemassege7: 'wow beautiful',
    replyname17: '__devil_kunju',
    replemassege17: 'cute....',
    replyname27: 'the_devil',
    replemassege27: 'happy family',
    replyname37: 'daddys_princess',
    replemassege37: 'always be happy',
    arrow7: require('../../../../../assets/images/Arrow.png'),
    close7: <CloseIcon />,
    commenting7:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
];

export default function Card() {
  const [popup, setPopup] = useState(false);
  const [likes, setLikes] = useState({});
  const [likeCounts, setLikeCounts] = useState({});
  const [animatedValue] = useState(new Animated.Value(0));
  const [key, setKey] = useState();
  const [showReplies, setShowReplies] = useState(false);
  const [showReplies1, setShowReplies1] = useState(false);
  const [showReplies2, setShowReplies2] = useState(false);
  const [showReplies3, setShowReplies3] = useState(false);
  const [showReplies4, setShowReplies4] = useState(false);

  const toggleReplies = () => {
    setShowReplies(!showReplies);
  };
  const toggleReplies1 = () => {
    setShowReplies1(!showReplies1);
  };
  const toggleReplies2 = () => {
    setShowReplies2(!showReplies2);
  };
  const toggleReplies3 = () => {
    setShowReplies3(!showReplies3);
  };
  const toggleReplies4 = () => {
    setShowReplies4(!showReplies4);
  };

  const animateModal = () => {
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 500,
      useNativeDriver: false,
    }).start();
  };

  const handleOpenModal = () => {
    setPopup(true);
    animateModal();
  };

  const handleLikeToggle = postId => {
    setLikes(prevLikes => ({
      ...prevLikes,
      [postId]: !prevLikes[postId],
    }));
    setLikeCounts(prevCounts => ({
      ...prevCounts,
      [postId]: (prevCounts[postId] || 0) + (likes[postId] ? -1 : 1),
    }));
  };

  return (
    <SafeAreaView style={styles.Main}>
      <View style={styles.Container}>
        <FlatList
          data={Post}
          showsVerticalScrollIndicator={false}
          renderItem={({item, index}) => (
            <View style={styles.Post}>
              <View style={styles.Top}>
                <View style={styles.Profile}>
                  <View style={styles.ProfileImg}>
                    <Image style={styles.ProfileImage} source={item.propic} />
                  </View>
                  <View style={styles.ProfileName}>
                    <Text style={styles.NameText}>{item.username}</Text>
                    <Text style={styles.PlaceText}>{item.userplace}</Text>
                  </View>
                </View>
                <View style={styles.Option}>
                  <MoreIcon />
                </View>
              </View>
              <View style={styles.Middle}>
                <Image style={styles.MiddleImg} source={item.postimg} />
              </View>
              <View style={styles.Bottom}>
                <View style={styles.Like}>
                  <TouchableOpacity
                    activeOpacity={0.8}
                    onPress={() => handleLikeToggle(item.id)}>
                    {likes[item.id] ? <LikeIcon /> : <HeartIcon />}
                  </TouchableOpacity>
                  <Text style={styles.LikeText}>
                    {likeCounts[item.id] !== undefined
                      ? likeCounts[item.id] + item.like
                      : item.like}
                  </Text>
                </View>
                <TouchableOpacity
                  onPress={() => {
                    handleOpenModal();
                    setKey(item.id);
                  }}
                  activeOpacity={0.8}
                  style={styles.Comment}>
                  <CommentIcon />
                  <Text style={styles.CommentText}>879</Text>
                </TouchableOpacity>
              </View>
              <TouchableOpacity style={styles.cmnt}>
                <Text style={styles.cmntText}>
                  <Text style={styles.cmntText1}>{item.text1}</Text>
                  {item.text2}
                </Text>
              </TouchableOpacity>
              <Modal
                animationType="slide"
                transparent={true}
                visible={popup && key === item.id}>
                <View style={styles.modalComment}>
                  <View style={styles.modalMain}>
                    <TouchableOpacity
                      activeOpacity={0.5}
                      style={styles.modalScroller}
                      onPress={() => setPopup(false)}>
                      <ScrollerIcon />
                    </TouchableOpacity>
                    <View style={styles.modalNew}>
                      <View style={styles.modalTop}>
                        <Text style={styles.modalText1}>Comments</Text>
                        <Text style={styles.modalText2}>{item.comment1}</Text>
                      </View>
                      <View style={styles.modalMiddle}>
                        <View style={styles.MiddleTop}>
                          <View style={styles.Profile1}>
                            <View style={styles.ProfileImg1}>
                              <Image
                                style={styles.ProfileImage1}
                                source={item.propic}
                              />
                            </View>
                            <View style={styles.ProfileName1}>
                              <Text style={styles.NameText1}>
                                {item.username}
                              </Text>
                              <Text style={styles.PlaceText1}>
                                {item.userplace}
                              </Text>
                            </View>
                            <View style={styles.Option1}>
                              {/* <MoreIcon /> */}
                            </View>
                          </View>
                        </View>
                        <View style={styles.MiddleBottom}>
                          <View style={styles.MiddleBottomComment}>
                            <Text style={styles.cmntText2}>
                              <Text style={styles.cmntText3}>
                                {item.wish1}{' '}
                              </Text>
                              {item.wish2}
                            </Text>
                          </View>
                          <ScrollView>
                            <View style={styles.modalBottom}>
                              <View key={item.id}>
                                <View style={styles.modalBottom1}>
                                  <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.ModalBottomTop}>
                                    <View style={styles.Profile}>
                                      <View style={styles.ProfileImg}>
                                        <Image
                                          style={styles.ProfileImage}
                                          source={item.commenterPropic}
                                        />
                                      </View>
                                      <View style={styles.ProfileName}>
                                        <Text style={styles.NameText}>
                                          {item.commentername}
                                        </Text>
                                        <Text style={styles.PlaceText}>
                                          {item.commentdate}
                                        </Text>
                                      </View>
                                    </View>
                                    <TouchableOpacity style={styles.Option}>
                                      <MoreIcon />
                                    </TouchableOpacity>
                                  </TouchableOpacity>
                                  <View style={styles.ModalBottomMiddle}>
                                    <Text style={styles.ModalBottomText}>
                                      {item.commenting1}
                                    </Text>
                                  </View>
                                </View>

                                <View style={styles.modalBottom1}>
                                  <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.ModalBottomTop}>
                                    <View style={styles.Profile}>
                                      <View style={styles.ProfileImg}>
                                        <Image
                                          style={styles.ProfileImage}
                                          source={item.commenterPropic1}
                                        />
                                      </View>
                                      <View style={styles.ProfileName}>
                                        <Text style={styles.NameText}>
                                          {item.commentername1}
                                        </Text>
                                        <Text style={styles.PlaceText}>
                                          {item.commentdate1}
                                        </Text>
                                      </View>
                                    </View>
                                    <TouchableOpacity style={styles.Option}>
                                      <MoreIcon />
                                    </TouchableOpacity>
                                  </TouchableOpacity>
                                  <View style={styles.ModalBottomMiddle}>
                                    <Text style={styles.ModalBottomText}>
                                      {item.commenting2}
                                    </Text>
                                  </View>
                                  {showReplies && (
                                    <TouchableOpacity
                                      activeOpacity={0.5}
                                      onPress={toggleReplies}
                                      style={styles.ModalBottomMiddle1}>
                                      <View
                                        style={styles.ModalBottomMiddle1Text}>
                                        <Text
                                          style={
                                            styles.ModalBottomMiddle1Texting
                                          }>
                                          {item.rply1}
                                        </Text>
                                        <View
                                          style={styles.ModalBottomMiddle1img}>
                                          <Image
                                            style={{
                                              width: 15,
                                              height: 15,
                                            }}
                                            source={item.arrow1}
                                          />
                                        </View>
                                      </View>
                                    </TouchableOpacity>
                                  )}

                                  {!showReplies && (
                                    <>
                                      <View style={styles.ModalReply}>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic1}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname1}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege1}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic11}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname11}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege11}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic21}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname21}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege21}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic31}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname31}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege31}
                                        </Text>
                                      </View>

                                      <TouchableOpacity
                                        onPress={toggleReplies}
                                        style={styles.Hide}>
                                        <Text style={styles.HideText}>
                                          ---Hide Reply---
                                        </Text>
                                      </TouchableOpacity>
                                    </>
                                  )}
                                </View>

                                <View style={styles.modalBottom1}>
                                  <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.ModalBottomTop}>
                                    <View style={styles.Profile}>
                                      <View style={styles.ProfileImg}>
                                        <Image
                                          style={styles.ProfileImage}
                                          source={item.commenterPropic2}
                                        />
                                      </View>
                                      <View style={styles.ProfileName}>
                                        <Text style={styles.NameText}>
                                          {item.commentername2}
                                        </Text>
                                        <Text style={styles.PlaceText}>
                                          {item.commentdate2}
                                        </Text>
                                      </View>
                                    </View>
                                    <TouchableOpacity style={styles.Option}>
                                      <MoreIcon />
                                    </TouchableOpacity>
                                  </TouchableOpacity>
                                  <View style={styles.ModalBottomMiddle}>
                                    <Text style={styles.ModalBottomText}>
                                      {item.commenting2}
                                    </Text>
                                  </View>

                                  {showReplies1 && (
                                    <TouchableOpacity
                                      activeOpacity={0.5}
                                      onPress={toggleReplies1}
                                      style={styles.ModalBottomMiddle1}>
                                      <View
                                        style={styles.ModalBottomMiddle1Text}>
                                        <Text
                                          style={
                                            styles.ModalBottomMiddle1Texting
                                          }>
                                          {item.rply2}
                                        </Text>
                                        <View
                                          style={styles.ModalBottomMiddle1img}>
                                          <Image
                                            style={{
                                              width: 15,
                                              height: 15,
                                            }}
                                            source={item.arrow2}
                                          />
                                        </View>
                                      </View>
                                    </TouchableOpacity>
                                  )}
                                  {!showReplies1 && (
                                    <>
                                      <View style={styles.ModalReply}>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic2}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname2}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege2}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic12}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname12}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege12}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic22}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname22}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege22}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic32}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname32}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege32}
                                        </Text>
                                      </View>

                                      <TouchableOpacity
                                        onPress={toggleReplies1}
                                        style={styles.Hide}>
                                        <Text style={styles.HideText}>
                                          ---Hide Reply---
                                        </Text>
                                      </TouchableOpacity>
                                    </>
                                  )}
                                </View>
                                <View style={styles.modalBottom1}>
                                  <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.ModalBottomTop}>
                                    <View style={styles.Profile}>
                                      <View style={styles.ProfileImg}>
                                        <Image
                                          style={styles.ProfileImage}
                                          source={item.commenterPropic3}
                                        />
                                      </View>
                                      <View style={styles.ProfileName}>
                                        <Text style={styles.NameText}>
                                          {item.commentername3}
                                        </Text>
                                        <Text style={styles.PlaceText}>
                                          {item.commentdate3}
                                        </Text>
                                      </View>
                                    </View>
                                    <TouchableOpacity style={styles.Option}>
                                      <MoreIcon />
                                    </TouchableOpacity>
                                  </TouchableOpacity>
                                  <View style={styles.ModalBottomMiddle}>
                                    <Text style={styles.ModalBottomText}>
                                      {item.commenting3}
                                    </Text>
                                  </View>
                                </View>
                                <View style={styles.modalBottom1}>
                                  <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.ModalBottomTop}>
                                    <View style={styles.Profile}>
                                      <View style={styles.ProfileImg}>
                                        <Image
                                          style={styles.ProfileImage}
                                          source={item.commenterPropic4}
                                        />
                                      </View>
                                      <View style={styles.ProfileName}>
                                        <Text style={styles.NameText}>
                                          {item.commentername4}
                                        </Text>
                                        <Text style={styles.PlaceText}>
                                          {item.commentdate4}
                                        </Text>
                                      </View>
                                    </View>
                                    <TouchableOpacity style={styles.Option}>
                                      <MoreIcon />
                                    </TouchableOpacity>
                                  </TouchableOpacity>
                                  <View style={styles.ModalBottomMiddle}>
                                    <Text style={styles.ModalBottomText}>
                                      {item.commenting4}
                                    </Text>
                                  </View>

                                  {showReplies2 && (
                                    <TouchableOpacity
                                      activeOpacity={0.5}
                                      onPress={toggleReplies2}
                                      style={styles.ModalBottomMiddle1}>
                                      <View
                                        style={styles.ModalBottomMiddle1Text}>
                                        <Text
                                          style={
                                            styles.ModalBottomMiddle1Texting
                                          }>
                                          {item.rply4}
                                        </Text>
                                        <View
                                          style={styles.ModalBottomMiddle1img}>
                                          <Image
                                            style={{
                                              width: 15,
                                              height: 15,
                                            }}
                                            source={item.arrow4}
                                          />
                                        </View>
                                      </View>
                                    </TouchableOpacity>
                                  )}
                                  {!showReplies2 && (
                                    <>
                                      <View style={styles.ModalReply}>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic4}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname4}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege4}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic14}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname14}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege14}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic24}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname24}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege24}
                                        </Text>
                                        <TouchableOpacity
                                          style={styles.flexrow}>
                                          <Image
                                            style={styles.RplyImage}
                                            source={item.relpyPropic34}
                                          />
                                          <Text style={styles.ModalReplyName}>
                                            {item.replyname34}
                                          </Text>
                                        </TouchableOpacity>
                                        <Text style={styles.ModalReplyText}>
                                          {item.replemassege34}
                                        </Text>
                                      </View>

                                      <TouchableOpacity
                                        onPress={toggleReplies2}
                                        style={styles.Hide}>
                                        <Text style={styles.HideText}>
                                          ---Hide Reply---
                                        </Text>
                                      </TouchableOpacity>
                                    </>
                                  )}
                                </View>

                                <View style={styles.modalBottom1}>
                                  <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.ModalBottomTop}>
                                    <View style={styles.Profile}>
                                      <View style={styles.ProfileImg}>
                                        <Image
                                          style={styles.ProfileImage}
                                          source={item.commenterPropic5}
                                        />
                                      </View>
                                      <View style={styles.ProfileName}>
                                        <Text style={styles.NameText}>
                                          {item.commentername5}
                                        </Text>
                                        <Text style={styles.PlaceText}>
                                          {item.commentdate5}
                                        </Text>
                                      </View>
                                    </View>
                                    <TouchableOpacity style={styles.Option}>
                                      <MoreIcon />
                                    </TouchableOpacity>
                                  </TouchableOpacity>
                                  <View style={styles.ModalBottomMiddle}>
                                    <Text style={styles.ModalBottomText}>
                                      {item.commenting5}
                                    </Text>
                                  </View>
                                </View>

                                <View style={styles.modalBottom1}>
                                  <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.ModalBottomTop}>
                                    <View style={styles.Profile}>
                                      <View style={styles.ProfileImg}>
                                        <Image
                                          style={styles.ProfileImage}
                                          source={item.commenterPropic6}
                                        />
                                      </View>
                                      <View style={styles.ProfileName}>
                                        <Text style={styles.NameText}>
                                          {item.commentername6}
                                        </Text>
                                        <Text style={styles.PlaceText}>
                                          {item.commentdate6}
                                        </Text>
                                      </View>
                                    </View>
                                    <TouchableOpacity style={styles.Option}>
                                      <MoreIcon />
                                    </TouchableOpacity>
                                  </TouchableOpacity>
                                  <View style={styles.ModalBottomMiddle}>
                                    <Text style={styles.ModalBottomText}>
                                      {item.commenting6}
                                    </Text>
                                  </View>

                                  {showReplies3 && (
                                    <TouchableOpacity
                                      activeOpacity={0.5}
                                      onPress={toggleReplies3}
                                    style={styles.ModalBottomMiddle1}>
                                    <View style={styles.ModalBottomMiddle1Text}>
                                      <Text
                                        style={
                                          styles.ModalBottomMiddle1Texting
                                        }>
                                        {item.rply6}
                                      </Text>
                                      <View
                                        style={styles.ModalBottomMiddle1img}>
                                        <Image
                                          style={{
                                            width: 15,
                                            height: 15,
                                          }}
                                          source={item.arrow6}
                                        />
                                      </View>
                                    </View>
                                  </TouchableOpacity>
                                  )}
                                   {!showReplies3 && (
                                  <>
                                    <View style={styles.ModalReply}>
                                      <TouchableOpacity style={styles.flexrow}>
                                        <Image
                                          style={styles.RplyImage}
                                          source={item.relpyPropic6}
                                        />
                                        <Text style={styles.ModalReplyName}>
                                          {item.replyname6}
                                        </Text>
                                      </TouchableOpacity>
                                      <Text style={styles.ModalReplyText}>
                                        {item.replemassege6}
                                      </Text>
                                      <TouchableOpacity style={styles.flexrow}>
                                        <Image
                                          style={styles.RplyImage}
                                          source={item.relpyPropic16}
                                        />
                                        <Text style={styles.ModalReplyName}>
                                          {item.replyname16}
                                        </Text>
                                      </TouchableOpacity>
                                      <Text style={styles.ModalReplyText}>
                                        {item.replemassege16}
                                      </Text>
                                      <TouchableOpacity style={styles.flexrow}>
                                        <Image
                                          style={styles.RplyImage}
                                          source={item.relpyPropic26}
                                        />
                                        <Text style={styles.ModalReplyName}>
                                          {item.replyname26}
                                        </Text>
                                      </TouchableOpacity>
                                      <Text style={styles.ModalReplyText}>
                                        {item.replemassege26}
                                      </Text>
                                      <TouchableOpacity style={styles.flexrow}>
                                        <Image
                                          style={styles.RplyImage}
                                          source={item.relpyPropic36}
                                        />
                                        <Text style={styles.ModalReplyName}>
                                          {item.replyname36}
                                        </Text>
                                      </TouchableOpacity>
                                      <Text style={styles.ModalReplyText}>
                                        {item.replemassege36}
                                      </Text>
                                    </View>

                                    <TouchableOpacity
                                       onPress={toggleReplies3}
                                      style={styles.Hide}>
                                      <Text style={styles.HideText}>
                                        ---Hide Reply---
                                      </Text>
                                    </TouchableOpacity>
                                  </>
                                   )}
                                </View>

                                <View style={styles.modalBottom1}>
                                  <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={styles.ModalBottomTop}>
                                    <View style={styles.Profile}>
                                      <View style={styles.ProfileImg}>
                                        <Image
                                          style={styles.ProfileImage}
                                          source={item.commenterPropic7}
                                        />
                                      </View>
                                      <View style={styles.ProfileName}>
                                        <Text style={styles.NameText}>
                                          {item.commentername7}
                                        </Text>
                                        <Text style={styles.PlaceText}>
                                          {item.commentdate7}
                                        </Text>
                                      </View>
                                    </View>
                                    <TouchableOpacity style={styles.Option}>
                                      <MoreIcon />
                                    </TouchableOpacity>
                                  </TouchableOpacity>
                                  <View style={styles.ModalBottomMiddle}>
                                    <Text style={styles.ModalBottomText}>
                                      {item.commenting7}
                                    </Text>
                                  </View>

                                  {showReplies4 && (
                                    <TouchableOpacity
                                      activeOpacity={0.5}
                                      onPress={toggleReplies4}
                                    style={styles.ModalBottomMiddle1}>
                                    <View style={styles.ModalBottomMiddle1Text}>
                                      <Text
                                        style={
                                          styles.ModalBottomMiddle1Texting
                                        }>
                                        {item.rply7}
                                      </Text>
                                      <View
                                        style={styles.ModalBottomMiddle1img}>
                                        {item.arrow7 && (
                                          <Image
                                            style={{
                                              width: 15,
                                              height: 15,
                                            }}
                                            source={item.arrow7}
                                          />
                                        )}
                                      </View>
                                    </View>
                                  </TouchableOpacity>
                                  )}
                                   {!showReplies3 && (
                                  <>
                                    <View style={styles.ModalReply}>
                                      <TouchableOpacity style={styles.flexrow}>
                                        <Image
                                          style={styles.RplyImage}
                                          source={item.relpyPropic7}
                                        />
                                        <Text style={styles.ModalReplyName}>
                                          {item.replyname7}
                                        </Text>
                                      </TouchableOpacity>
                                      <Text style={styles.ModalReplyText}>
                                        {item.replemassege7}
                                      </Text>
                                      <TouchableOpacity style={styles.flexrow}>
                                        <Image
                                          style={styles.RplyImage}
                                          source={item.relpyPropic17}
                                        />
                                        <Text style={styles.ModalReplyName}>
                                          {item.replyname17}
                                        </Text>
                                      </TouchableOpacity>
                                      <Text style={styles.ModalReplyText}>
                                        {item.replemassege17}
                                      </Text>
                                      <TouchableOpacity style={styles.flexrow}>
                                        <Image
                                          style={styles.RplyImage}
                                          source={item.relpyPropic27}
                                        />
                                        <Text style={styles.ModalReplyName}>
                                          {item.replyname27}
                                        </Text>
                                      </TouchableOpacity>
                                      <Text style={styles.ModalReplyText}>
                                        {item.replemassege27}
                                      </Text>
                                      <TouchableOpacity style={styles.flexrow}>
                                        <Image
                                          style={styles.RplyImage}
                                          source={item.relpyPropic37}
                                        />
                                        <Text style={styles.ModalReplyName}>
                                          {item.replyname37}
                                        </Text>
                                      </TouchableOpacity>
                                      <Text style={styles.ModalReplyText}>
                                        {item.replemassege37}
                                      </Text>
                                    </View>

                                    <TouchableOpacity
                                      onPress={toggleReplies4}
                                      style={styles.Hide}>
                                      <Text style={styles.HideText}>
                                        ---Hide Reply---
                                      </Text>
                                    </TouchableOpacity>
                                  </>
                                   )}
                                </View>
                              </View>
                            </View>
                          </ScrollView>
                        </View>
                      </View>
                    </View>
                  </View>
                </View>
              </Modal>
            </View>
          )}
          keyExtractor={item => item.id.toString()}
        />
      </View>
    </SafeAreaView>
  );
}
const styles = StyleSheet.create({
  Main: {
    borderTopWidth: 1,
    borderColor: '#EEEEEE',
    marginTop: SIZE(20),
    borderRadius: 5,
    backgroundColor: '#EEEEEE',
  },
  Container: {
    backgroundColor: '#EEEEEE',
  },
  Post: {
    backgroundColor: '#ffff',
    marginBottom: SIZE(15),
  },
  Top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: SIZE(10),
    paddingVertical: SIZE(10),
  },
  Profile1: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: SIZE(10),
  },
  ProfileImg: {
    borderWidth: 1,
    borderColor: '#EF767A',
    backgroundColor: '#ffff',
    borderRadius: 17,
    width: 36,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ProfileImage1: {
    width: 34,
    height: 34,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ProfileName1: {
    marginLeft: SIZE(10),
  },
  NameText1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
  },
  PlaceText1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 11,
    fontWeight: '700',
    color: '#747474',
  },
  Option1: {},
  Profile: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: SIZE(10),
  },
  ProfileImg: {
    borderWidth: 1,
    borderColor: '#EF767A',
    backgroundColor: '#ffff',
    borderRadius: 17,
    width: 34,
    height: 34,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ProfileImage: {
    width: 29,
    height: 29,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ProfileName: {
    marginLeft: SIZE(10),
  },
  NameText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
  },
  PlaceText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 11,
    fontWeight: '700',
    color: '#747474',
  },
  Option: {},
  Middle: {
    paddingHorizontal: SIZE(12),
    paddingVertical: SIZE(8),
  },
  MiddleImg: {
    width: SIZE(396),
    height: SIZE(397),
    borderRadius: 10,
  },
  Bottom: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: SIZE(15),
    paddingVertical: SIZE(10),
  },
  Like: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  LikeText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    fontWeight: '700',
    color: '#707070',
    marginLeft: SIZE(5),
  },
  Comment: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: SIZE(10),
  },
  CommentText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    fontWeight: '700',
    color: '#707070',
    marginLeft: SIZE(5),
  },
  cmnt: {
    paddingHorizontal: SIZE(15),
  },
  cmntText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '700',
    color: '#707070',
    marginLeft: SIZE(5),
    marginBottom: SIZE(20),
  },
  cmntText1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
    marginLeft: SIZE(5),
  },
  modalComment: {
    flex: 1,
    backgroundColor: '#eeee',
    marginBottom: SIZE(110),
  },
  modalMain: {
    backgroundColor: '#fff',
    borderRadius: 20,
    paddingHorizontal: SIZE(20),
    paddingTop: SIZE(15),
    marginTop: SIZE(85),
  },
  modalScroller: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: SIZE(30),
  },
  modalTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: SIZE(20),
  },
  modalText1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: '700',
    color: '#272727',
  },
  modalText2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    fontWeight: '700',
    color: '#459F56',
  },
  modalMiddle: {},
  MiddleTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: SIZE(20),
  },

  MiddleBottom: {
    borderBottomWidth: 1,
    borderColor: '#EEEEEE',
  },
  MiddleBottomComment: {},
  cmntText2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '700',
    color: '#707070',
    marginLeft: SIZE(5),
    marginBottom: SIZE(20),
  },
  cmntText3: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
    marginLeft: SIZE(5),
  },
  modalBottom: {
    paddingTop: SIZE(30),
    paddingBottom: SIZE(400),
    marginBottom: SIZE(450),
  },
  ModalBottomTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 5,
  },
  ModalBottomText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '700',
    color: '#272727',
    marginLeft: 5,
    marginBottom: 10,
    paddingLeft: 50,
  },
  ModalBottomMiddle1: {
    flexDirection: 'row',
    paddingLeft: 50,
    paddingBottom: 20,
  },
  ModalBottomMiddle1Text: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ModalBottomMiddle1Texting: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#272727',
    marginRight: 1,
  },
  ModalBottomMiddle1img: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  ModalReply: {
    paddingLeft: SIZE(50),
    paddingBottom: SIZE(0),
  },
  ModalReplyName: {
    paddingLeft: SIZE(10),
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#272727',
  },
  ModalReplyText: {
    paddingLeft: SIZE(40),
    paddingBottom: SIZE(15),
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#272727',
  },
  flexrow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: SIZE(2),
  },
  RplyImage: {
    width: 25,
    height: 25,
    borderRadius: 13,
    alignItems: 'center',
    justifyContent: 'center',
  },
  Hide: {
    paddingLeft: SIZE(50),
    paddingBottom: SIZE(15),
  },
  HideText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 12,
    fontWeight: '700',
    color: '#747474',
  },
  modalBottom1: {},
});
