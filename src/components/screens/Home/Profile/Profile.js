import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  ScrollView,
  Modal,
  Animated,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import React, {useState, useEffect} from 'react';
import {useFocusEffect} from '@react-navigation/native';
import axios from 'axios';
import Posts from './Posts/Posts';

import HamberIcon from '../../../../assets/icons/Hamberger.svg';
import PlusIcon from '../../../../assets/icons/Pluss.svg';
import BackIcon from '../../../../assets/icons/ArrowLeft.svg';
import {SIZE} from '../../../../styles';

const Photos = [
  {
    id: 1,
    image: require('../../../../assets/images/library1.png'),
  },
  {
    id: 2,
    image: require('../../../../assets/images/library2.png'),
  },
  {
    id: 3,
    image: require('../../../../assets/images/library3.png'),
  },
  {
    id: 4,
    image: require('../../../../assets/images/library4.png'),
  },
  {
    id: 5,
    image: require('../../../../assets/images/library5.png'),
  },
  {
    id: 6,
    image: require('../../../../assets/images/library6.png'),
  },
  {
    id: 7,
    image: require('../../../../assets/images/library7.png'),
  },
  {
    id: 8,
    image: require('../../../../assets/images/library8.png'),
  },
  {
    id: 9,
    image: require('../../../../assets/images/library9.png'),
  },
];

export default function Profile({navigation}) {
  const [numColumns, setNumColumns] = useState(3);
  const keyExtractor = (_, index) => `${index}-${numColumns}`;
  const [animatedValue] = useState(new Animated.Value(0));
  const [popup, setPopup] = useState(false);
  const [selected, setSelected] = useState(1);
  const [profileData, setProfileData] = useState({name: '', profile: null});
  const [products, setProducts] = useState([]);

  useFocusEffect(
    React.useCallback(() => {
      const retrieveProfileData = async () => {
        try {
          const storedName = await AsyncStorage.getItem('name');
          const storedProfile = await AsyncStorage.getItem('profileImage');

          const defaultName = 'Sophiya jacob';
          const defaultProfile = require('../../../../assets/images/Sophia.png');

          const name = storedName || defaultName;
          const profile = storedProfile || defaultProfile;

          setProfileData({name, profile});
        } catch (error) {
          console.error('Error retrieving profile data:', error);
        }
      };

      retrieveProfileData();
    }, []),
  );

  const {name, profile} = profileData;

  const resetCache = async () => {
    try {
      await AsyncStorage.clear();
      navigation.navigate('Signup');
    } catch (error) {
      console.error('Error clearing profile data:', error);
    }
  };

  const animateModal = () => {
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 500,
      useNativeDriver: false,
    }).start();
  };

  const handleOpenModal = () => {
    setPopup(true);
    animateModal();
  };
  const goBack = () => {
    navigation.goBack();
  };

  useEffect(() => {
    fetchProducts();
  }, []);

  const fetchProducts = async () => {
    try {
      const response = await axios.get('https://fakestoreapi.com/products');
      setProducts(response.data);
    } catch (error) {
      console.error('Error fetching products:', error);
    }
  };
  const deleteProduct = async (productId) => {
    try {
      console.log('Deleting product with ID:', productId);
      await axios.delete(`https://fakestoreapi.com/products/${productId}`);

      setProducts(prevProducts => prevProducts.filter(product => product.id !== productId));
    } catch (error) {
      console.error('Error deleting product:', error);
    }
  };

  return (
    <SafeAreaView style={styles.Main}>
      <View style={styles.Container}>
        <View style={styles.Header}>
          <ImageBackground
            source={require('../../../../assets/images/Frame.png')}
            style={styles.Background}
            resizeMode="cover">
            <View style={styles.HeaderTop}>
              <Text style={styles.HeaderTitle}>{name}</Text>
              <TouchableOpacity onPress={() => navigation.navigate('Settings')}>
                <HamberIcon />
              </TouchableOpacity>
            </View>
            <View style={styles.HeaderBottom}>
              <View style={styles.HeaderProfile}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={styles.HeaderProfilePd}>
                  <Image
                    style={styles.HeaderProfileImg}
                    source={
                      profile && typeof profile === 'string'
                        ? {uri: profile}
                        : require('../../../../assets/images/Sophia.png')
                    }
                  />
                </TouchableOpacity>
                <TouchableOpacity activeOpacity={0.8} style={styles.HeaderPlus}>
                  <PlusIcon />
                </TouchableOpacity>
              </View>
              <View style={styles.HeaderName}>
                <Text style={styles.ProfileName}>{name}</Text>
                <Text style={styles.ProfilePlace}>
                  Thalirath Family,
                  <Text style={styles.House}> Kattappana, Idukki</Text>
                </Text>
              </View>
              <View style={styles.HeaderMembers}>
                <View style={styles.MembersImg}>
                  <Image
                    style={styles.MembersImg1}
                    source={require('../../../../assets/images/House.png')}
                  />
                  <Image
                    style={styles.MembersImg2}
                    source={require('../../../../assets/images/House.png')}
                  />
                  <Image
                    style={styles.MembersImg3}
                    source={require('../../../../assets/images/House.png')}
                  />
                </View>
                <Text style={styles.MembersText}>House Members</Text>
              </View>
            </View>
          </ImageBackground>
        </View>

        <View style={styles.Body}>
          <View style={styles.BodyTop}>
            <TouchableOpacity
              activeOpacity={0.8}
              style={styles.Edit}
              onPress={() => navigation.navigate('EditProfile')}>
              <Text style={styles.EditText}>Edit Profile</Text>
            </TouchableOpacity>
          </View>
        </View>
        <ScrollView>
          <View style={styles.Library}>
            <FlatList
              data={products}
              showsVerticalScrollIndicator={true}
              numColumns={3}
              keyExtractor={item => item.id.toString()}
              renderItem={({item, index}) => (
                <View style={styles.Gallery}>
                  <TouchableOpacity
                    activeOpacity={0.6}
                    onPress={() =>
                      navigation.navigate('Posts', {
                        selectedImage: {uri: item.image},
                        title: item.title,
                        description: item.description,
                        deleteProduct: deleteProduct,
                      })
                    }
                    >
                     
                    <Image style={styles.Image} source={{uri: item.image}} />
                  </TouchableOpacity>
                </View>
              )}
            />
          </View>
        </ScrollView>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: '#ffff',
    justifyContent: 'space-between',
  },
  Header: {},
  Background: {
    width: SIZE(415),
    height: SIZE(282),
  },
  HeaderTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: SIZE(20),
    paddingVertical: SIZE(15),
  },
  HeaderTitle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
  },
  HeaderProfile: {
    paddingTop: SIZE(20),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  HeaderProfilePd: {
    borderWidth: 2,
    borderColor: '#59CC6E',
    borderRadius: 99,
    padding: 1,
  },
  HeaderProfileImg: {
    width: SIZE(119.59),
    height: SIZE(119.59),
    borderRadius: 99,
  },
  HeaderPlus: {
    width: 23.29,
    height: 23.29,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#ffff',
    borderRadius: 50,
    backgroundColor: '#59CC6E',
    position: 'absolute',
    top: SIZE(110),
    right: SIZE(150),
  },
  HeaderName: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  ProfileName: {
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: '700',
    color: '#272727',
  },
  ProfilePlace: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#459F56',
  },
  House: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#979797',
  },
  HeaderMembers: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: SIZE(15),
  },
  MembersImg: {
    flexDirection: 'row',
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
  MembersText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 11,
    fontWeight: '700',
    color: '#747474',
    width: SIZE(81),
    left: SIZE(85),
  },
  MembersImg1: {
    position: 'absolute',
    left: -30,
    zIndex: 0,
  },
  MembersImg2: {
    position: 'absolute',
    left: 0,
    zIndex: 10,
  },
  MembersImg3: {
    position: 'absolute',
    left: 30,
    zIndex: 20,
  },
  Body: {
    marginTop: 20,
    paddingVertical: 25,
    paddingHorizontal: 20,
  },
  BodyTop: {},
  Edit: {
    backgroundColor: '#F3F3F3',
    paddingHorizontal: 20,
    paddingVertical: 15,
    marginBottom: 10,
    borderRadius: 10,
  },
  EditText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    fontWeight: '700',
    color: '#000',
    textAlign: 'center',
  },
  Library: {
    paddingHorizontal: 20,
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
    paddingBottom: 220,
    marginBottom: 220,
  },
  Gallery: {
    marginBottom: 5,
    marginRight: 5,
    justifyContent: 'space-between',
  },
  Image: {
    width: SIZE(120),
    height: SIZE(120),
    aspectRatio: 1,
    borderRadius: 10,
  },
});
