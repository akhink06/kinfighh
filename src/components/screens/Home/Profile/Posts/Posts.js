import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
  FlatList,
  Image,
  Animated,
  Modal,
} from 'react-native';
import React, {useState} from 'react';

import BackIcon from '../../../../../assets/icons/ArrowLeft.svg';
import MoreIcon from '../../../../../assets/icons/more.svg';
import LikeIcon from '../../../../../assets/icons/Like.svg';
import HeartIcon from '../../../../../assets/icons/Heart.svg';
import CommentIcon from '../../../../../assets/icons/Comment.svg';
import ScrollerIcon from '../../../../../assets/icons/Scroller.svg';
import CloseIcon from '../../../../../assets/icons/close.svg';
import EditPostIcon from '../../../../../assets/icons/Pencile.svg';
import ShareIcon from '../../../../../assets/icons/Share.svg';
import DeleteIcon from '../../../../../assets/icons/Delete.svg';
import { SIZE } from '../../../../../styles';

const Post = [
  {
    id: 1,
    propic: require('../../../../../assets/images/pro.png'),
    username: 'David Richard',
    userplace: 'Thekkan Family',
    postimg: require('../../../../../assets/images/Rec.png'),
    likeimg: <LikeIcon />,
    like: 1499,
    commentimg: <CommentIcon />,
    comment: 879,
    text1: 'Birthday Celebration ',
    text2:
      'amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui eget justo tincidunt amet consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ante. Proin dapibus dui eget justo tincidunt',
  },
];

const Comments = [
  {
    id: 1,
    commenterPropic: require('../../../../../assets/images/Maccoy.png'),
    commentername: 'Corina McCoy',
    commentdate: '2 days ago',
    close: <CloseIcon />,
    comment:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
  {
    id: 2,
    commenterPropic: require('../../../../../assets/images/Alex.png'),
    commentername: 'Alex Buckmaster',
    commentdate: '2 days ago',
    rply: '--- View 13 replay',
    arrow: require('../../../../../assets/images/Arrow.png'),
    close: <CloseIcon />,
    comment:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
  },
  {
    id: 3,
    commenterPropic: require('../../../../../assets/images/Philips.png'),
    commentername: 'Autumn Philips',
    commentdate: '2 days ago',
    rply: '--- View 44 replay',
    arrow: require('../../../../../assets/images/Arrow.png'),
    close: <CloseIcon />,
    comment:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
  {
    id: 4,
    commenterPropic: require('../../../../../assets/images/Maccoy.png'),
    commentername: 'Corina McCoy',
    commentdate: '2 days ago',
    close: <CloseIcon />,
    comment:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
  {
    id: 5,
    commenterPropic: require('../../../../../assets/images/Maccoy.png'),
    commentername: 'Corina McCoy',
    commentdate: '2 days ago',
    rply: '--- View 22 replay',
    arrow: require('../../../../../assets/images/Arrow.png'),
    close: <CloseIcon />,
    comment:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
  {
    id: 6,
    commenterPropic: require('../../../../../assets/images/Alex.png'),
    commentername: 'Alex Buckmaster',
    commentdate: '2 days ago',
    close: <CloseIcon />,
    comment:
      'met consectetur accumsan, nibh exsollicit udin metus, volutpat lacinia arcu nibh vexl ',
  },
  {
    id: 7,
    commenterPropic: require('../../../../../assets/images/Philips.png'),
    commentername: 'Autumn Philips',
    commentdate: '2 days ago',
    rply: '--- View 30 replay',
    arrow: require('../../../../../assets/images/Arrow.png'),
    close: <CloseIcon />,
    comment:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
  {
    id: 8,
    commenterPropic: require('../../../../../assets/images/Maccoy.png'),
    commentername: 'Corina McCoy',
    commentdate: '2 days ago',
    rply: '--- View 19 replay',
    arrow: require('../../../../../assets/images/Arrow.png'),
    close: <CloseIcon />,
    comment:
      'Fusce volutpat lectus et nisl consectetur finibus. In vitae scelerisque augue, in varius eros. Nunc sapien diam, euismod et pretium id, ',
  },
];
export default function Posts({navigation,route }) {
    const { selectedImage , title , description , deleteProduct} = route.params;
  const [popup, setPopup] = useState(false);
  const [popup1, setPopup1] = useState(false);
  const [popup2, setPopup2] = useState(false);
  const [likes, setLikes] = useState({});
  const [likeCounts, setLikeCounts] = useState({});
  const [animatedValue] = useState(new Animated.Value(0));

  const animateModal = () => {
    Animated.timing(animatedValue, {
      toValue: 1,
      duration: 500,
      useNativeDriver: false,
    }).start();
  };

  const handleOpenModal = () => {
    setPopup(true);
    animateModal();
  };
  const handleOpenModal1 = () => {
    setPopup1(false);
    setPopup2(true);
    animateModal();
  };

  const updatePopup = action => {
    switch (action.type) {
      case 'CLOSE':
        setPopup2(false);
        break;
      default:
        break;
    }
  };

  const handleLikeToggle = postId => {
    setLikes(prevLikes => ({
      ...prevLikes,
      [postId]: !prevLikes[postId],
    }));
    setLikeCounts(prevCounts => ({
      ...prevCounts,
      [postId]: (prevCounts[postId] || 0) + (likes[postId] ? -1 : 1),
    }));
  };

  const goBack = () => {
    navigation.goBack();
  };

  
  const handleDelete = () => {
    if (typeof deleteProduct === 'function') {
      deleteProduct();
    }
  };


  return (
    <SafeAreaView style={styles.Main}>
      <View style={styles.Container}>
        <View style={styles.Header}>
          <View style={styles.HeaderTop}>
            <TouchableOpacity onPress={() => navigation.navigate('Profile')} activeOpacity={0.5}>
              <BackIcon />
            </TouchableOpacity>
            <Text style={styles.HeaderTitle}>Posts</Text>
            <View></View>
          </View>
          <View style={styles.Body}>
            <FlatList
              data={Post}
              showsVerticalScrollIndicator={false}
              renderItem={({item, index}) => (
                <View style={styles.Post}>
                  <View style={styles.Top}>
                    <View style={styles.Profile}>
                      <View style={styles.ProfileImg}>
                        <Image
                          style={styles.ProfileImage}
                          source={item.propic}
                        />
                      </View>
                      <View style={styles.ProfileName}>
                        <Text style={styles.NameText}>{item.username}</Text>
                        <Text style={styles.PlaceText}>{item.userplace}</Text>
                      </View>
                    </View>
                    <View style={styles.Option}>
                      <TouchableOpacity
                        onPress={() => setPopup1(true)}
                        activeOpacity={0.6}>
                        <MoreIcon />
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={styles.Middle}>
                    <Image style={styles.MiddleImg} source={selectedImage} />
                  </View>
                  <View style={styles.Bottom}>
                    <View style={styles.Like}>
                      <TouchableOpacity
                        activeOpacity={0.8}
                        onPress={() => handleLikeToggle(item.id)}>
                        {likes[item.id] ? <LikeIcon /> : <HeartIcon />}
                      </TouchableOpacity>
                      <Text style={styles.LikeText}>
                        {likeCounts[item.id] !== undefined
                          ? likeCounts[item.id] + item.like
                          : item.like}
                      </Text>
                    </View>
                    <TouchableOpacity
                      onPress={handleOpenModal}
                      activeOpacity={0.8}
                      style={styles.Comment}>
                      <CommentIcon />
                      <Text style={styles.CommentText}>879</Text>
                    </TouchableOpacity>
                  </View>
                  <TouchableOpacity style={styles.cmnt}>
                    <Text style={styles.cmntText}>
                      <Text style={styles.cmntText1}>{title}</Text>
                      {description}
                    </Text>
                  </TouchableOpacity>
                </View>
              )}
              keyExtractor={item => item.id.toString()}
            />
            <Modal animationType="slide" transparent={true} visible={popup}>
              <View style={styles.modalComment}>
                <View style={styles.modalMain}>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.modalScroller}
                    onPress={() => setPopup(false)}>
                    <ScrollerIcon />
                  </TouchableOpacity>
                  <View style={styles.modalTop}>
                    <Text style={styles.modalText1}>Comments</Text>
                    <Text style={styles.modalText2}>23</Text>
                  </View>
                  <View style={styles.modalMiddle}>
                    <View style={styles.MiddleTop}>
                      <View style={styles.Profile}>
                        <View style={styles.ProfileImg}>
                          <Image
                            style={styles.ProfileImage}
                            source={require('../../../../../assets/images/pro.png')}
                          />
                        </View>
                        <View style={styles.ProfileName}>
                          <Text style={styles.NameText}>David Richard</Text>
                          <Text style={styles.PlaceText}>Thekkan Family</Text>
                        </View>
                      </View>
                      <View style={styles.Option}>
                        <MoreIcon />
                      </View>
                    </View>
                    <View style={styles.MiddleBottom}>
                      <View style={styles.MiddleBottomComment}>
                        <Text style={styles.cmntText2}>
                          <Text style={styles.cmntText3}>
                            Birthday Celebration{' '}
                          </Text>
                          amet consectetur accumsan, nibh exsollicit udin metus,
                          volutpat lacinia arcu nibh vexl ante. Proin dapibus
                          dui eget justo tincidunt amet consectetur accumsan,
                          nibh exsollicit udin metus, volutpat lacinia arcu nibh
                          vexl ante. Proin dapibus dui eget justo tincidunt
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.modalBottom}>
                    <FlatList
                      data={Comments}
                      showsVerticalScrollIndicator={false}
                      renderItem={({item}) => (
                        <View style={styles.modalBottom1}>
                          <TouchableOpacity
                            activeOpacity={0.8}
                            style={styles.ModalBottomTop}>
                            <View style={styles.Profile}>
                              <View style={styles.ProfileImg}>
                                <Image
                                  style={styles.ProfileImage}
                                  source={item.commenterPropic}
                                />
                              </View>
                              <View style={styles.ProfileName}>
                                <Text style={styles.NameText}>
                                  {item.commentername}
                                </Text>
                                <Text style={styles.PlaceText}>
                                  {item.commentdate}
                                </Text>
                              </View>
                            </View>
                            <TouchableOpacity style={styles.Option}>
                              <MoreIcon />
                            </TouchableOpacity>
                          </TouchableOpacity>
                          <View style={styles.ModalBottomMiddle}>
                            <Text style={styles.ModalBottomText}>
                              {item.comment}
                            </Text>
                          </View>
                          <TouchableOpacity
                            activeOpacity={0.5}
                            style={styles.ModalBottomMiddle1}>
                            <View style={styles.ModalBottomMiddle1Text}>
                              <Text style={styles.ModalBottomMiddle1Texting}>
                                {item.rply}
                              </Text>
                              <View style={styles.ModalBottomMiddle1img}>
                                {item.arrow && (
                                  <Image
                                    style={{
                                      width: 15,
                                      height: 15,
                                    }}
                                    source={item.arrow}
                                  />
                                )}
                              </View>
                            </View>
                          </TouchableOpacity>
                        </View>
                      )}
                    />
                  </View>
                </View>
              </View>
            </Modal>
            <Modal animationType="slide" transparent={true} visible={popup1}>
              <View style={styles.modalComment1}>
                <View style={styles.modalMain1}>
                  <TouchableOpacity
                    activeOpacity={0.5}
                    style={styles.modalScroller}
                    onPress={() => setPopup1(false)}>
                    <ScrollerIcon />
                  </TouchableOpacity>
                  <View style={styles.Delete}>
                    <TouchableOpacity
                      activeOpacity={0.6}
                      style={styles.EditProfile}>
                      <EditPostIcon />
                      <Text style={styles.EditProfileText}>Edit Post</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      activeOpacity={0.6}
                      style={styles.Account}>
                      <ShareIcon />
                      <Text style={styles.AccountText}>Share Post</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      onPress={() => {
                        handleOpenModal1(), updatePopup;
                      }}
                      activeOpacity={0.6}
                      style={styles.Privacy}>
                      <DeleteIcon />
                      <Text style={styles.PrivacyText}>Delete Post</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
            <Modal animationType="slide" transparent={true} visible={popup2}>
              <View style={styles.modalComment2}>
                <View style={styles.modalMain2}>
                  <View style={styles.modal2}>
                    <Text style={styles.DeletePost}>
                      Are you sure to delete your post?
                    </Text>
                    <View style={styles.modalBottom1}>
                      <TouchableOpacity
                        activeOpacity={0.6}
                        onPress={() => {
                          navigation.navigate('Profile');
                          handleDelete();
                        }}   style={styles.DeletePost1}>
                        <Text style={styles.DeletePost1Text}>Delete now</Text>
                      </TouchableOpacity>
                      <TouchableOpacity
                        activeOpacity={0.6}
                        onPress={goBack}
                        style={styles.DeletePost2}>
                        <Text style={styles.DeletePost2Text}>No</Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  Container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  Header: {
    flex: 1,
  },
  HeaderTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderColor: '#EEEEEE',
  },
  HeaderTitle: {
    justifyContent: 'flex-start',
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
  },
  Body: {
    // paddingHorizontal: 20,
    paddingVertical: SIZE(20),
  },
  Post: {
    backgroundColor: '#ffff',
    marginBottom: SIZE(15),
  },
  Top: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: SIZE(20),
    paddingVertical: SIZE(20),
  },
  Profile: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingLeft: SIZE(10),
  },
  ProfileImg: {
    borderWidth: 1,
    borderColor: '#EF767A',
    backgroundColor: '#ffff',
    borderRadius: 17,
    width: 34,
    height: 34,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ProfileImage: {
    width: 29,
    height: 29,
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ProfileName: {
    marginLeft: SIZE(10),
  },
  NameText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
  },
  PlaceText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 11,
    fontWeight: '700',
    color: '#747474',
  },
  Option: {},
  Middle: {
    paddingHorizontal: SIZE(8),
    paddingVertical: SIZE(5),
  },
  MiddleImg: {
    width: SIZE(396),
    height: SIZE(397),
    borderRadius: 10,
  },
  Bottom: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  Like: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  LikeText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    fontWeight: '700',
    color: '#707070',
    marginLeft: 5,
  },
  Comment: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 10,
  },
  CommentText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 13,
    fontWeight: '700',
    color: '#707070',
    marginLeft: 5,
  },
  cmnt: {
    paddingHorizontal: 15,
  },
  cmntText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '700',
    color: '#707070',
    marginLeft: 5,
    marginBottom: 20,
  },
  cmntText1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
    marginLeft: 5,
  },
  modalComment: {
    flex: 1,
    backgroundColor: '#eeee',
  },
  modalMain: {
    backgroundColor: '#fff',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingTop: 15,
    marginTop: 85,
  },
  modalScroller: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 30,
  },
  modalTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },
  modalText1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: '700',
    color: '#272727',
  },
  modalText2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 18,
    fontWeight: '700',
    color: '#459F56',
  },
  modalMiddle: {},
  MiddleTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 20,
  },

  MiddleBottom: {
    borderBottomWidth: 1,
    borderColor: '#EEEEEE',
  },
  MiddleBottomComment: {},
  cmntText2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '700',
    color: '#707070',
    marginLeft: 5,
    marginBottom: 20,
  },
  cmntText3: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000',
    marginLeft: 5,
  },
  modalBottom: {
    paddingTop: 30,
    paddingBottom: 250,
    marginBottom: 250,
  },
  ModalBottomTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 5,
  },
  ModalBottomText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '700',
    color: '#272727',
    marginLeft: 5,
    marginBottom: 10,
    paddingLeft: 50,
  },
  ModalBottomMiddle1: {
    flexDirection: 'row',
    paddingLeft: 50,
    paddingBottom: 20,
  },
  ModalBottomMiddle1Text: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  ModalBottomMiddle1Texting: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#272727',
    marginRight: 1,
  },
  ModalBottomMiddle1img: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  EditProfile: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderTopRightRadius: 9,
    borderTopLeftRadius: 9,
  },
  EditProfileText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000000',
    paddingLeft: 15,
  },
  Account: {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderTopWidth: 3,
    borderBottomWidth: 3,
    borderColor: '#ffff',
  },
  AccountText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000000',
    paddingLeft: 15,
  },
  Privacy: {
    marginBottom: 60,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderBottomRightRadius: 9,
    borderBottomLeftRadius: 9,
  },
  PrivacyText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#E01507',
    paddingLeft: 15,
  },
  modalComment1: {
    flex: 1,
    backgroundColor: 'rgba(238, 238, 238, 0.5)',
  },
  modalMain1: {
    backgroundColor: '#fff',
    borderRadius: SIZE(20),
    paddingHorizontal:SIZE(20),
    paddingTop: 15,
    marginTop: SIZE(540),
  },
  Delete: {
    paddingHorizontal: 10,
  },
  modalComment2: {
    flex: 1,
    backgroundColor: 'rgba(238, 238, 238, 0.5)',
  },
  modalMain2: {
    flex: 1,
    paddingHorizontal:SIZE(10),
    backgroundColor: 'rgba(238, 238, 238, 0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modal2 : {
    backgroundColor: '#fff',
    paddingHorizontal: SIZE(15),
    paddingVertical : SIZE(40),
    borderRadius : SIZE(15),
  },
  DeletePost : {
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: '700',
    color: '#000000',
    textAlign : 'center',
    lineHeight : 30,
    paddingHorizontal:80,
  },
  modalBottom1 : {
    marginTop : 20,
  },
  DeletePost1 : {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderTopRightRadius: 9,
    borderTopLeftRadius: 9,
    justifyContent : 'center',
    borderBottomWidth : 3,
    borderColor : '#ffff',
  },
  DeletePost1Text : {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#E02B1D',
    paddingLeft: 15,
  },
  DeletePost2 : {
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F6F6F6',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderBottomRightRadius: 9,
    borderBottomLeftRadius: 9,
    justifyContent : 'center',
  },
  DeletePost2Text : {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#747474',
    paddingLeft: 15,
  },
});
