import React, {useState , useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import DateTimePickerModal from '@react-native-community/datetimepicker';
import AsyncStorage from '@react-native-async-storage/async-storage';

import ImagePicker from 'react-native-image-crop-picker';

import Profile from '../../Profile/Profile';

import CloseIcon from '../../../../../assets/icons/EditClose.svg';
import CameraIcon from '../../../../../assets/icons/Camera.svg';
import CalenderIcon from '../../../../../assets/icons/Calender.svg';
import {SIZE} from '../../../../../styles';

export default function EditProfile({navigation}) {
  const [selectedGender, setSelectedGender] = useState(null);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [name, setName] = useState('');
  const [profile, setProfile] = useState(null);

  useEffect(() => {
    loadProfileImage();
  }, []);

  const loadProfileImage = async () => {
    try {
      const storedProfile = await AsyncStorage.getItem('profileImage');
      if (storedProfile) {
        setProfile(storedProfile);
      }
    } catch (error) {
      console.log('Error loading profile image:', error);
    }
  };

  const saveForm = async () => {
    try {
      if (profile !== null && profile !== undefined) {
        await AsyncStorage.setItem('profileImage', profile);
      }
      await AsyncStorage.setItem('name', name);
      navigation.navigate('Profile');
    } catch (error) {
      console.log('Error saving form:', error);
    }
  };
  
  const showDatePicker = () => {
    setDatePickerVisibility(!isDatePickerVisible);
  };

  const handleConfirm = date => {
    if (date) {
      setSelectedDate(date);
    }
    setDatePickerVisibility(false);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleGenderSelection = gender => {
    setSelectedGender(gender);
  };

  const goBack = () => {
    navigation.goBack();
  };

  const Imagepick = () => {
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true,
    })
      .then(image => {
        console.log('Selected image:', image.path); 
        setProfile(image.path); 
      })
      .catch(error => {
        console.log('Error picking image:', error);
      });
  };
  
  return (
    <SafeAreaView style={styles.Main}>
      <View style={styles.Container}>
        <View style={styles.Header}>
          <Text style={styles.HeaderTitle}>Edit Profile</Text>
          <TouchableOpacity activeOpacity={0.6} onPress={goBack}>
            <CloseIcon />
          </TouchableOpacity>
        </View>
        <View style={styles.Body}>
          <View style={styles.Top}>
            <View style={styles.Profile}>
              <TouchableOpacity activeOpacity={0.6} style={styles.ProfilePd}>
              <Image
                  style={styles.ProfileImg}
                  source={
                    profile
                      ? { uri: profile }
                      : require('../../../../../assets/images/Sophia.png')
                  }
                />
              </TouchableOpacity>
              <TouchableOpacity
                onPress={Imagepick}
                activeOpacity={0.8}
                style={styles.Camera}>
                <CameraIcon />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.Middle}>
            <View style={styles.Name}>
              <Text style={styles.Label}>Enter Name</Text>
              <TextInput
                style={styles.TextInput}
                placeholder="User name"
                placeholderTextColor={'#747474'}
                onChangeText={setName}
                value={name}
              />
            </View>
            <View style={styles.Gender}>
              <Text style={styles.Label}>Select Gender</Text>
              <View style={styles.GenderRadio}>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={[
                    styles.GenderOption1,
                    selectedGender === 'male' && styles.ActiveOption,
                  ]}
                  onPress={() => handleGenderSelection('male')}>
                  <View
                    style={[
                      styles.Circle,
                      selectedGender === 'male' && styles.ActiveCircle,
                    ]}>
                    {selectedGender === 'male' && (
                      <View style={styles.InnerCircle}></View>
                    )}
                  </View>
                  <Text
                    style={[
                      styles.GenderText,
                      selectedGender === 'male' && styles.ActiveGenderText,
                    ]}>
                    Male
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={[
                    styles.GenderOption2,
                    selectedGender === 'female' && styles.ActiveOption,
                  ]}
                  onPress={() => handleGenderSelection('female')}>
                  <View
                    style={[
                      styles.Circle,
                      selectedGender === 'female' && styles.ActiveCircle,
                    ]}>
                    {selectedGender === 'female' && (
                      <View style={styles.InnerCircle}></View>
                    )}
                  </View>
                  <Text
                    style={[
                      styles.GenderText,
                      selectedGender === 'female' && styles.ActiveGenderText,
                    ]}>
                    Female
                  </Text>
                </TouchableOpacity>
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={[
                    styles.GenderOption3,
                    selectedGender === 'other' && styles.ActiveOption,
                  ]}
                  onPress={() => handleGenderSelection('other')}>
                  <View
                    style={[
                      styles.Circle,
                      selectedGender === 'other' && styles.ActiveCircle,
                    ]}>
                    {selectedGender === 'other' && (
                      <View style={styles.InnerCircle}></View>
                    )}
                  </View>
                  <Text
                    style={[
                      styles.GenderText,
                      selectedGender === 'other' && styles.ActiveGenderText,
                    ]}>
                    Other
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.DOB}>
              <Text style={styles.Label}>Enter birth date</Text>
              <TouchableOpacity activeOpacity={0.6} onPress={showDatePicker}>
                <View style={styles.TextInput1}>
                  <Text style={styles.TextInput2}>
                    {selectedDate.toDateString()}
                  </Text>
                  <CalenderIcon />
                </View>
              </TouchableOpacity>
              {isDatePickerVisible && (
                <DateTimePickerModal
                  isVisible={isDatePickerVisible}
                  mode="date"
                  value={selectedDate}
                  onChange={(event, date) => handleConfirm(date)}
                  onCancel={hideDatePicker}
                />
              )}
            </View>
          </View>
        </View>
        <View style={styles.Footer}>
          <TouchableOpacity
            title="Show Toast"
            onPress={saveForm}
            activeOpacity={0.8}
            style={styles.Save}>
            <Text style={styles.SaveText}>Save</Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={goBack}
            activeOpacity={0.8}
            style={styles.Cancel}>
            <Text style={styles.CancelText}>Cancel</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: '#ffff',
    justifyContent: 'space-between',
  },
  Container: {
    justifyContent: 'space-between',
  },
  Header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: SIZE(20),
    paddingVertical: SIZE(15),
    borderBottomWidth: 1,
    borderColor: '#EEEEEE',
  },
  HeaderTitle: {
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
  },
  Profile: {
    paddingTop: SIZE(40),
    paddingBottom: SIZE(15),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
  },
  ProfilePd: {
    borderTopWidth: 1,
    borderRightWidth: 0,
    borderBottomWidth: 0,
    borderLeftWidth: 2,
    borderColor: '#EF767A',
    borderRadius: 99,
    padding: SIZE(5),
  },
  ProfileImg: {
    width: 160.8,
    height: 160.8,
    borderRadius: 99,
  },
  Camera: {
    width: 40.8,
    height: 40.8,
    alignItems: 'center',
    justifyContent: 'center',
    borderWidth: 2,
    borderColor: '#ffff',
    borderRadius: 50,
    backgroundColor: '#283B88',
    position: 'absolute',
    top: SIZE(180),
    right: SIZE(120),
  },
  Middle: {
    paddingHorizontal: SIZE(20),
    paddingVertical: SIZE(15),
  },
  Label: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#747474',
    paddingBottom: SIZE(5),
    paddingLeft: SIZE(20),
  },
  TextInput: {
    borderWidth: 1,
    borderColor: '#D4D4D4',
    borderRadius: 99,
    paddingHorizontal: SIZE(25),
    paddingVertical: SIZE(10),
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000000',
    marginBottom: SIZE(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  TextInput1: {
    borderWidth: 1,
    borderColor: '#D4D4D4',
    borderRadius: 99,
    paddingHorizontal: SIZE(25),
    paddingVertical: SIZE(13),
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#000000',
    marginBottom: SIZE(20),
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  TextInput2: {
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    fontWeight: '700',
    color: '#747474',
  },
  Gender: {
    marginBottom: SIZE(20),
  },
  GenderRadio: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: SIZE(20),
    paddingVertical: SIZE(15),
    borderBottomWidth: 1,
    borderColor: '#D4D4D4',
    borderWidth: 1,
    borderRadius: 99,
  },
  Male: {
    borderRightWidth: 1,
    borderColor: '#D4D4D4',
  },
  Circle: {
    width: 20,
    height: 20,
    borderRadius: 20 / 2,
    borderWidth: 1,
    borderColor: '#747474',
    justifyContent: 'center',
    alignItems: 'center',
  },
  ActiveCircle: {
    width: 20,
    height: 20,
    borderRadius: 20 / 2,
    backgroundColor: '#ffff',
    borderColor: '#459F56',
    justifyContent: 'center',
    alignItems: 'center',
  },
  InnerCircle: {
    width: 8.64,
    height: 8.64,
    borderRadius: 8.64 / 2,
    backgroundColor: '#459F56',
  },
  GenderOption1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRightWidth: 1,
    borderColor: '#D4D4D4',
    paddingRight: SIZE(20),
    paddingLeft: SIZE(10),
  },
  GenderOption2: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderRightWidth: 1,
    borderColor: '#D4D4D4',
    paddingRight: SIZE(20),
  },
  GenderOption3: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingRight: SIZE(20),
  },
  Female: {},
  Other: {},
  GenderText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#747474',
    paddingLeft: SIZE(10),
  },
  ActiveGenderText: {
    color: '#000',
  },
  Footer: {
    paddingHorizontal: SIZE(20),
    paddingVertical: SIZE(15),
    marginTop: SIZE(20),
    paddingTop: SIZE(10),
  },
  Save: {
    backgroundColor: '#459F56',
    paddingHorizontal: SIZE(20),
    paddingVertical: SIZE(15),
    borderRadius: 99,
    marginBottom: SIZE(20),
  },
  SaveText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#ffff',
    textAlign: 'center',
  },
  Cancel: {
    paddingHorizontal: SIZE(20),
    paddingVertical: SIZE(5),
    borderRadius: 99,
  },
  CancelText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#E02B1D',
    textAlign: 'center',
  },
});


