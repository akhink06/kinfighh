import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import React from 'react';

import BackIcon from '../../../../../assets/icons/ArrowLeft.svg';
import RightArrowIcon from '../../../../../assets/icons/Arrow.svg';

export default function AccountManagement({navigation}) {
  const goBack = () => {
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.Main}>
      <View style={styles.Container}>
        <View style={styles.Header}>
          <View style={styles.HeaderTop}>
            <TouchableOpacity onPress={goBack} activeOpacity={0.5}>
              <BackIcon />
            </TouchableOpacity>
            <Text style={styles.HeaderTitle}>Account Management</Text>
            <View></View>
          </View>
          <View style={styles.Body}>
            <Text style={styles.BodyText}>
            Make changes to phone number and account type. This information is private and won’t show up in your public profile.
            </Text>
            <View style={styles.Button}>
              <TouchableOpacity activeOpacity={0.6} style={styles.Deactivate} >
                <Text style={styles.ButtonText1}>Deactivate Account</Text>
                <RightArrowIcon />
              </TouchableOpacity>
              <TouchableOpacity activeOpacity={0.6} style={styles.Delete} >
                <Text style={styles.ButtonText1}>Delete Account</Text>
                <RightArrowIcon />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  Container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  Header: {
    flex: 1,
  },
  HeaderTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderColor: '#EEEEEE',
  },
  HeaderTitle: {
    justifyContent: 'flex-start',
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
  },
  Body: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  BodyText : {
    width: '94%',
    fontFamily: 'Poppins-Regular',
    fontSize: 14,
    lineHeight: 22,
    fontWeight: '700',
    color: '#747474',
    marginBottom: 20,
  },
  Button: {},
  ButtonText: {},
  Deactivate: {
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F8F8F8',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderRadius: 9,
    justifyContent:'space-between',
  },
  Delete: {
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F8F8F8',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderRadius: 9,
    justifyContent:'space-between',
  },
  ButtonText1: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000000',
  },

});
