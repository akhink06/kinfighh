import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import React, { useCallback } from 'react';

import AsyncStorage from '@react-native-async-storage/async-storage';

import BackIcon from '../../../../assets/icons/ArrowLeft.svg';
import EditIcon from '../../../../assets/icons/Content.svg';
import AccountIcon from '../../../../assets/icons/Users.svg';
import PrivacyIcon from '../../../../assets/icons/Archive.svg';
import LogoutIcon from '../../../../assets/icons/logout.svg';
import { useFocusEffect } from '@react-navigation/native';

export default function Settings({route, navigation}) {
  const goBack = () => {
    navigation.goBack();
  };

  const handleLogout = async () => {
    try {
      route.params.handleLogout();
    } catch (error) {
      console.error('Error logging out:', error);
    }
  };

  return (
    <SafeAreaView style={styles.Main}>
      <View style={styles.Container}>
        <View style={styles.Header}>
          <View style={styles.HeaderTop}>
            <TouchableOpacity onPress={goBack} activeOpacity={0.5}>
              <BackIcon />
            </TouchableOpacity>
            <Text style={styles.HeaderTitle}>Settings</Text>
            <View></View>
          </View>
          <View style={styles.Body}>
            <TouchableOpacity
              onPress={() => navigation.navigate('EditProfile')}
              activeOpacity={0.6}
              style={styles.EditProfile}>
              <EditIcon />
              <Text style={styles.EditProfileText}>Edit Profile</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('AccountManagement')}
              activeOpacity={0.6}
              style={styles.Account}>
              <AccountIcon />
              <Text style={styles.AccountText}>Account Management</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => navigation.navigate('PrivacyPolicy')}
              activeOpacity={0.6}
              style={styles.Privacy}>
              <PrivacyIcon />
              <Text style={styles.PrivacyText}>Privacy Policy</Text>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.Footer}>
          <TouchableOpacity
            activeOpacity={0.6}
            onPress={handleLogout}
            style={styles.Logout}>
            <LogoutIcon />
            <Text style={styles.LogoutText}>Logout</Text>
          </TouchableOpacity>
          <View></View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  Main: {
    flex: 1,
    backgroundColor: '#ffff',
  },
  Container: {
    flex: 1,
    justifyContent: 'space-between',
  },
  Header: {
    flex: 1,
  },
  HeaderTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderBottomWidth: 1,
    borderColor: '#EEEEEE',
  },
  HeaderTitle: {
    justifyContent: 'flex-start',
    fontFamily: 'Poppins-Regular',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000000',
  },
  Body: {
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  EditProfile: {
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F8F8F8',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderRadius: 9,
  },
  EditProfileText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000000',
    paddingLeft: 15,
  },
  Account: {
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F8F8F8',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderRadius: 9,
  },
  AccountText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000000',
    paddingLeft: 15,
  },
  Privacy: {
    marginBottom: 15,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: '#F8F8F8',
    paddingVertical: 20,
    paddingHorizontal: 15,
    borderRadius: 9,
  },
  PrivacyText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#000000',
    paddingLeft: 15,
  },
  Footer: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  Logout: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
  LogoutText: {
    fontFamily: 'Poppins-Regular',
    fontSize: 16,
    fontWeight: '700',
    color: '#E02B1D',
    paddingLeft: 15,
  },
});