import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';

import Feeds from '../screens/Home/Feeds/Feeds';
import Profile from '../screens/Home/Profile/Profile';
import New from '../screens/Home/New/New';
import Notification from '../screens/Home/Notification/Notification';
import Add from '../screens/Home/Add/Add';
import Card from '../screens/Home/Feeds/Inner_components/Card';
import Settings from '../screens/Home/Settings/Settings';
import AccountManagement from '../screens/Home/Settings/Inner_component/AccountManagement';
import PrivacyPolicy from '../screens/Home/Settings/Inner_component/PrivacyPolicy';
import Posts from '../screens/Home/Profile/Posts/Posts';
import Bording from '../screens/Auth/Boarding/Bording';
import UserStack from './UserStack';

import HomeIcon from '../../assets/icons/bottomhome.svg';
import HomeFocused from '../../assets/icons/HomeFocused.svg';
import NotificationIcon from '../../assets/icons/bottomnotification.svg';
import NotificationFocused from '../../assets/icons/NotificationFocused.svg';
import PlusIcon from '../../assets/icons/bottomroundplus.svg';
import TreeIcon from '../../assets/icons/bottomtree.svg';
import TreeFocused from '../../assets/icons/TreeFocused.svg';
import ProfileIcon from '../../assets/icons/bottomprofile.svg';
import ProfileFocused from '../../assets/icons/ProfileFocused.svg';
import EditProfile from '../screens/Home/Settings/Inner_component/EditProfile';
import SignUp from '../screens/Auth/Signup/SignUp';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();

const FeedsStack = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="Feeds" component={Feeds} />
      <Stack.Screen name="Profile" component={Profile} />
      <Stack.Screen name="New" component={New} />
      <Stack.Screen name="Notification" component={Notification} />
      <Stack.Screen name="Add" component={Add} />
      <Stack.Screen name="Card" component={Card} />
      <Stack.Screen name="AccountManagement" component={AccountManagement} />
      <Stack.Screen name="PrivacyPolicy" component={PrivacyPolicy} />
      <Stack.Screen name="Posts" component={Posts} />
    </Stack.Navigator>
  );
};

const TabNavigator = () => {
  return (
    <Tab.Navigator 
      initialRouteName="Feeds"
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused }) => {
          let iconComponent;
          if (route.name === 'Feeds') {
            iconComponent = focused ? (
              <HomeFocused style={styles.iconStyle} />
            ) : (
              <HomeIcon style={styles.iconStyle} />
            );
          } else if (route.name === 'New') {
            iconComponent = focused ? (
              <TreeFocused style={styles.iconStyle} />
            ) : (
              <TreeIcon style={styles.iconStyle} />
            );
          } else if (route.name === 'Add') {
            iconComponent = <PlusIcon style={styles.iconStyle} />;
          } else if (route.name === 'Notification') {
            iconComponent = focused ? (
              <NotificationFocused style={styles.iconStyle} />
            ) : (
              <NotificationIcon style={styles.iconStyle} />
            );
          } else if (route.name === 'Profile') {
            iconComponent = focused ? (
              <ProfileFocused style={styles.iconStyle} />
            ) : (
              <ProfileIcon style={styles.iconStyle} />
            );
          }
          return iconComponent;
        },
        tabBarShowLabel: false,
        tabBarStyle: {
          position: 'absolute',
          bottom: 0,
          left: 0,
          right: 0,
        },
        tabBarIconStyle: {
          marginBottom: 10,
          marginTop: 10,
        },
      })}
    >
      <Tab.Screen options={{ headerShown: false }} name="Feeds" component={FeedsStack} />
      <Tab.Screen options={{ headerShown: false }} name="New" component={New} />
      <Tab.Screen options={{ headerShown: false }} name="Add" component={Add} />
      <Tab.Screen options={{ headerShown: false }} name="Notification" component={Notification} />
      <Tab.Screen options={{ headerShown: false }} name="Profile" component={Profile} />
    </Tab.Navigator>
  );
};

const HomeStack = ({route}) => {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="TabNavigator" component={TabNavigator} />
      <Stack.Screen name="UserStack" component={UserStack} />
      <Stack.Screen
        name="Settings"
        component={Settings}
        initialParams={{
          isLoggedIn: route.params.isLoggedIn,
          handleLogout: route.params.handleLogout,
        }}
      />
      <Stack.Screen name="EditProfile" component={EditProfile} />
    </Stack.Navigator>
  );
};

export default HomeStack;

const styles = {
  iconStyle: {},
};