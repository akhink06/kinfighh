import { StyleSheet } from 'react-native';
import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Bording from '../screens/Auth/Boarding/Bording';
import HomeStack from './HomeStack';
import SignUp from '../screens/Auth/Signup/SignUp';

const Stack = createStackNavigator();

const AuthStack = ({ handleLoginSuccess }) => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }} initialRouteName="Bording">
      <Stack.Screen name="Bording" component={Bording} />
      <Stack.Screen name="SignUp">
        {(props) => <SignUp {...props} handleLoginSuccess={handleLoginSuccess} />}
      </Stack.Screen>
    </Stack.Navigator>
  );
};

export default AuthStack;

const styles = StyleSheet.create({});