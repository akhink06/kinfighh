import { StyleSheet, Text, View } from 'react-native'
import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import Profile from '../screens/Home/Profile/Profile';
import Settings from '../screens/Home/Settings/Settings';
import AccountManagement from '../screens/Home/Settings/Inner_component/AccountManagement';
import EditProfile from '../screens/Home/Settings/Inner_component/EditProfile';
import PrivacyPolicy from '../screens/Home/Settings/Inner_component/PrivacyPolicy';
import Posts from '../screens/Home/Profile/Posts/Posts';


const Stack = createStackNavigator();

export default function UserStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}} initialRouteName='Profile'>
    <Stack.Screen name="Profile" component={Profile} />
    <Stack.Screen name="Posts" component={Posts} />
    <Stack.Screen name="EditProfile" component={EditProfile} />
    <Stack.Screen name="Settings" component={Settings} />
  </Stack.Navigator>
  )
}

const styles = StyleSheet.create({})