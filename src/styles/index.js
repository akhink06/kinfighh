import { StyleSheet } from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const SIZE = value => {
  return wp(value / 4.2);
};

export {SIZE}