// App.js
import 'react-native-gesture-handler';
import React, { useEffect, useState } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from 'react-native-splash-screen';
import AsyncStorage from '@react-native-async-storage/async-storage';
import AuthStack from './src/components/routing/AuthStack';
import HomeStack from './src/components/routing/HomeStack';

const Stack = createStackNavigator();

export default function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    checkUserAuthentication();
  }, []);

  const checkUserAuthentication = async () => {
    try {
      const loggedIn = await AsyncStorage.getItem('isLoggedIn');
      setIsLoggedIn(loggedIn === 'true');
    } catch (error) {
      console.log('Error checking user authentication:', error);
    } finally {
      SplashScreen.hide();
    }
  };

  const handleLoginSuccess = async (userId, phoneNumber, navigation) => {
    try {
      await AsyncStorage.setItem('userId', userId);
      await AsyncStorage.setItem('phoneNumber', phoneNumber);

      await AsyncStorage.setItem('isLoggedIn', 'true');
      await checkUserAuthentication()

    } catch (error) {
      console.error('Error saving user data:', error);
    }
  };

    const handleLogout = async () => {
      try {
        await AsyncStorage.clear();
        setIsLoggedIn(false);
      } catch (error) {
        console.error('Error logging out:', error);
      }
    };

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {isLoggedIn ? (
          <Stack.Screen
            name="HomeStack"
            component={HomeStack}
            options={{headerShown: false}}
            initialParams={{isLoggedIn: isLoggedIn, handleLogout: handleLogout}}
          />
        ) : (
          <Stack.Screen name="AuthStack" options={{headerShown: false}}>
            {props => (
              <AuthStack {...props} handleLoginSuccess={handleLoginSuccess} />
            )}
          </Stack.Screen>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
}